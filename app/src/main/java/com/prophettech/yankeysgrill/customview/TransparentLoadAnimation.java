/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.customview;

import android.app.Activity;
import android.app.Dialog;

import com.prophettech.yankeysgrill.R;

import java.lang.ref.WeakReference;

public class TransparentLoadAnimation {

    private static final String TAG = TransparentLoadAnimation.class.getName();
    private Dialog mDialog;
    private WeakReference<Activity> mWeakReference;

    public TransparentLoadAnimation(Activity context) {
        if (context == null) {
            return;
        }
        this.mDialog = new Dialog(context, R.style.TransparentLoadingAnimation);
        this.mDialog.setContentView(R.layout.loader_animation);
        this.mDialog.setCancelable(false);
        mWeakReference = new WeakReference<>(context);
    }

    public boolean isShowing() {
        if (mDialog == null) {
            return false;
        }
        return mDialog.isShowing();
    }

    public final void showProgressView() {
        if (mWeakReference == null || mWeakReference.get() == null) {
            return;
        }

        Activity activity = mWeakReference.get();
        if (mDialog == null) {
            return;
        }
        if (!activity.isFinishing() && !mDialog.isShowing()) {
            mDialog.show();
        }
    }

    public final void hideProgressView() {
        if (mWeakReference == null || mWeakReference.get() == null) {
            return;
        }

        Activity activity = mWeakReference.get();
        if (mDialog == null) {
            return;
        }

        if (!activity.isFinishing()) {
            mDialog.dismiss();
        }
    }
}
