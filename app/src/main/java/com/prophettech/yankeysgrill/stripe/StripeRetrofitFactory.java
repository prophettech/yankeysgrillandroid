/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:17
 *
 */

package com.prophettech.yankeysgrill.stripe;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prophettech.yankeysgrill.models.retrofit.ArrayObjectDualityDeserializer;
import com.prophettech.yankeysgrill.models.retrofit.MetaDatum;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StripeRetrofitFactory {

    private static final String BASE_URL = "https://yankeysgrill.co.uk/";
    private static Retrofit retrofit;
    private static StripeRetrofitFactory myClient;

    private StripeRetrofitFactory() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static synchronized StripeRetrofitFactory getInstance() {
        if (myClient == null) {
            myClient = new StripeRetrofitFactory();
        }
        return myClient;
    }

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .registerTypeAdapter(MetaDatum.class, new ArrayObjectDualityDeserializer())
                    .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(300, TimeUnit.SECONDS)
                    .readTimeout(300, TimeUnit.SECONDS).build();

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public StripeBackendApi getMyApi() {
        return retrofit.create(StripeBackendApi.class);
    }
}
