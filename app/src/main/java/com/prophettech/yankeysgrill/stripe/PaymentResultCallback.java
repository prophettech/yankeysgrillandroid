/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.stripe;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prophettech.yankeysgrill.activities.CheckOutActivity;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.model.PaymentIntent;

import java.lang.ref.WeakReference;
import java.util.Objects;

public class PaymentResultCallback implements ApiResultCallback<PaymentIntentResult> {
    @NonNull
    private final WeakReference<CheckOutActivity> activityRef;

    public PaymentResultCallback(@NonNull CheckOutActivity activity) {
        activityRef = new WeakReference<>(activity);
    }

    @Override
    public void onSuccess(@NonNull PaymentIntentResult result) {
        final CheckOutActivity activity = activityRef.get();
        if (activity == null) {
            return;
        }

        PaymentIntent paymentIntent = result.getIntent();
        PaymentIntent.Status status = paymentIntent.getStatus();
        if (status == PaymentIntent.Status.Succeeded) {
            // Payment completed successfully
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            activity.showToastMessage("Payment completed! Please wait while we confirm your order!");
            activity.placeOrderOnWordPress();
        } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
            // Payment failed
            activity.showToastMessage(
                    "Payment failed!  " +
                            Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage()
            );
        }
    }

    @Override
    public void onError(@NonNull Exception e) {
        final CheckOutActivity activity = activityRef.get();
        if (activity == null) {
            return;
        }

        // Payment request failed – allow retrying using the same payment method
        activity.showToastMessage("Error! " + e.toString());
    }
}
