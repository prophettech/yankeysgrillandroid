/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.stripe;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface StripeBackendApi {

    @FormUrlEncoded
    @POST("stripebackend/createpaymentintent.php")
    Call<ResponseBody> createPaymentIntent(@Field("amount") int amount, @Field("currency") String currency);
}

