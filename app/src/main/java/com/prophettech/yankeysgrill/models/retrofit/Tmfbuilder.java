
/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.models.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Tmfbuilder {

    @SerializedName("section_header_size")
    @Expose
    private List<String> sectionHeaderSize = null;
    @SerializedName("section_divider_type")
    @Expose
    private List<String> sectionDividerType = null;
    @SerializedName("sections")
    @Expose
    private List<String> sections = null;
    @SerializedName("selectbox_internal_name")
    @Expose
    private List<String> selectboxInternalName = null;
    @SerializedName("selectbox_header_size")
    @Expose
    private List<String> selectboxHeaderSize = null;
    @SerializedName("selectbox_header_title")
    @Expose
    private List<String> selectboxHeaderTitle = null;
    @SerializedName("selectbox_header_title_position")
    @Expose
    private List<String> selectboxHeaderTitlePosition = null;
    @SerializedName("selectbox_header_title_color")
    @Expose
    private List<String> selectboxHeaderTitleColor = null;
    @SerializedName("selectbox_header_subtitle")
    @Expose
    private List<String> selectboxHeaderSubtitle = null;
    @SerializedName("selectbox_header_subtitle_position")
    @Expose
    private List<String> selectboxHeaderSubtitlePosition = null;
    @SerializedName("selectbox_header_subtitle_color")
    @Expose
    private List<String> selectboxHeaderSubtitleColor = null;
    @SerializedName("selectbox_divider_type")
    @Expose
    private List<String> selectboxDividerType = null;
    @SerializedName("selectbox_enabled")
    @Expose
    private List<String> selectboxEnabled = null;
    @SerializedName("selectbox_required")
    @Expose
    private List<String> selectboxRequired = null;
    @SerializedName("selectbox_text_before_price")
    @Expose
    private List<String> selectboxTextBeforePrice = null;
    @SerializedName("selectbox_text_after_price")
    @Expose
    private List<String> selectboxTextAfterPrice = null;
    @SerializedName("selectbox_price_type")
    @Expose
    private List<String> selectboxPriceType = null;
    @SerializedName("selectbox_hide_amount")
    @Expose
    private List<String> selectboxHideAmount = null;
    @SerializedName("selectbox_quantity")
    @Expose
    private List<String> selectboxQuantity = null;
    @SerializedName("selectbox_quantity_min")
    @Expose
    private List<String> selectboxQuantityMin = null;
    @SerializedName("selectbox_quantity_max")
    @Expose
    private List<String> selectboxQuantityMax = null;
    @SerializedName("selectbox_quantity_step")
    @Expose
    private List<String> selectboxQuantityStep = null;
    @SerializedName("selectbox_quantity_default_value")
    @Expose
    private List<String> selectboxQuantityDefaultValue = null;
    @SerializedName("selectbox_placeholder")
    @Expose
    private List<String> selectboxPlaceholder = null;
    @SerializedName("selectbox_use_url")
    @Expose
    private List<String> selectboxUseUrl = null;
    @SerializedName("selectbox_changes_product_image")
    @Expose
    private List<String> selectboxChangesProductImage = null;
    @SerializedName("multiple_selectbox_options_title")
    @Expose
    private List<List<String>> multipleSelectboxOptionsTitle = null;
    @SerializedName("multiple_selectbox_options_image")
    @Expose
    private List<List<String>> multipleSelectboxOptionsImage = null;
    @SerializedName("multiple_selectbox_options_imagec")
    @Expose
    private List<List<String>> multipleSelectboxOptionsImagec = null;
    @SerializedName("multiple_selectbox_options_imagep")
    @Expose
    private List<List<String>> multipleSelectboxOptionsImagep = null;
    @SerializedName("multiple_selectbox_options_imagel")
    @Expose
    private List<List<String>> multipleSelectboxOptionsImagel = null;
    @SerializedName("multiple_selectbox_options_value")
    @Expose
    private List<List<String>> multipleSelectboxOptionsValue = null;
    @SerializedName("multiple_selectbox_options_price")
    @Expose
    private List<List<String>> multipleSelectboxOptionsPrice = null;
    @SerializedName("multiple_selectbox_options_sale_price")
    @Expose
    private List<List<String>> multipleSelectboxOptionsSalePrice = null;
    @SerializedName("multiple_selectbox_options_price_type")
    @Expose
    private List<List<String>> multipleSelectboxOptionsPriceType = null;
    @SerializedName("multiple_selectbox_options_description")
    @Expose
    private List<List<String>> multipleSelectboxOptionsDescription = null;
    @SerializedName("multiple_selectbox_options_url")
    @Expose
    private List<List<String>> multipleSelectboxOptionsUrl = null;
    @SerializedName("selectbox_uniqid")
    @Expose
    private List<String> selectboxUniqid = null;
    @SerializedName("selectbox_clogic")
    @Expose
    private List<String> selectboxClogic = null;
    @SerializedName("selectbox_logic")
    @Expose
    private List<String> selectboxLogic = null;
    @SerializedName("selectbox_class")
    @Expose
    private List<String> selectboxClass = null;
    @SerializedName("selectbox_container_id")
    @Expose
    private List<String> selectboxContainerId = null;
    @SerializedName("selectbox_include_tax_for_fee_price_type")
    @Expose
    private List<String> selectboxIncludeTaxForFeePriceType = null;
    @SerializedName("selectbox_tax_class_for_fee_price_type")
    @Expose
    private List<String> selectboxTaxClassForFeePriceType = null;
    @SerializedName("selectbox_hide_element_label_in_cart")
    @Expose
    private List<String> selectboxHideElementLabelInCart = null;
    @SerializedName("selectbox_hide_element_value_in_cart")
    @Expose
    private List<String> selectboxHideElementValueInCart = null;
    @SerializedName("selectbox_hide_element_label_in_order")
    @Expose
    private List<String> selectboxHideElementLabelInOrder = null;
    @SerializedName("selectbox_hide_element_value_in_order")
    @Expose
    private List<String> selectboxHideElementValueInOrder = null;
    @SerializedName("selectbox_hide_element_label_in_floatbox")
    @Expose
    private List<String> selectboxHideElementLabelInFloatbox = null;
    @SerializedName("selectbox_hide_element_value_in_floatbox")
    @Expose
    private List<String> selectboxHideElementValueInFloatbox = null;
    @SerializedName("checkboxes_internal_name")
    @Expose
    private List<String> checkboxesInternalName = null;
    @SerializedName("checkboxes_header_size")
    @Expose
    private List<String> checkboxesHeaderSize = null;
    @SerializedName("checkboxes_header_title")
    @Expose
    private List<String> checkboxesHeaderTitle = null;
    @SerializedName("checkboxes_header_title_position")
    @Expose
    private List<String> checkboxesHeaderTitlePosition = null;
    @SerializedName("checkboxes_header_title_color")
    @Expose
    private List<String> checkboxesHeaderTitleColor = null;
    @SerializedName("checkboxes_header_subtitle")
    @Expose
    private List<String> checkboxesHeaderSubtitle = null;
    @SerializedName("checkboxes_header_subtitle_position")
    @Expose
    private List<String> checkboxesHeaderSubtitlePosition = null;
    @SerializedName("checkboxes_header_subtitle_color")
    @Expose
    private List<String> checkboxesHeaderSubtitleColor = null;
    @SerializedName("checkboxes_divider_type")
    @Expose
    private List<String> checkboxesDividerType = null;
    @SerializedName("checkboxes_enabled")
    @Expose
    private List<String> checkboxesEnabled = null;
    @SerializedName("checkboxes_required")
    @Expose
    private List<String> checkboxesRequired = null;
    @SerializedName("checkboxes_text_before_price")
    @Expose
    private List<String> checkboxesTextBeforePrice = null;
    @SerializedName("checkboxes_text_after_price")
    @Expose
    private List<String> checkboxesTextAfterPrice = null;
    @SerializedName("checkboxes_hide_amount")
    @Expose
    private List<String> checkboxesHideAmount = null;
    @SerializedName("checkboxes_quantity")
    @Expose
    private List<String> checkboxesQuantity = null;
    @SerializedName("checkboxes_quantity_min")
    @Expose
    private List<String> checkboxesQuantityMin = null;
    @SerializedName("checkboxes_quantity_max")
    @Expose
    private List<String> checkboxesQuantityMax = null;
    @SerializedName("checkboxes_quantity_step")
    @Expose
    private List<String> checkboxesQuantityStep = null;
    @SerializedName("checkboxes_quantity_default_value")
    @Expose
    private List<String> checkboxesQuantityDefaultValue = null;
    @SerializedName("checkboxes_limit_choices")
    @Expose
    private List<String> checkboxesLimitChoices = null;
    @SerializedName("checkboxes_exactlimit_choices")
    @Expose
    private List<String> checkboxesExactlimitChoices = null;
    @SerializedName("checkboxes_minimumlimit_choices")
    @Expose
    private List<String> checkboxesMinimumlimitChoices = null;
    @SerializedName("checkboxes_use_images")
    @Expose
    private List<String> checkboxesUseImages = null;
    @SerializedName("checkboxes_use_lightbox")
    @Expose
    private List<String> checkboxesUseLightbox = null;
    @SerializedName("checkboxes_swatchmode")
    @Expose
    private List<String> checkboxesSwatchmode = null;
    @SerializedName("checkboxes_use_colors")
    @Expose
    private List<String> checkboxesUseColors = null;
    @SerializedName("checkboxes_changes_product_image")
    @Expose
    private List<String> checkboxesChangesProductImage = null;
    @SerializedName("checkboxes_items_per_row")
    @Expose
    private List<String> checkboxesItemsPerRow = null;
    @SerializedName("checkboxes_items_per_row_tablets")
    @Expose
    private List<String> checkboxesItemsPerRowTablets = null;
    @SerializedName("checkboxes_items_per_row_tablets_small")
    @Expose
    private List<String> checkboxesItemsPerRowTabletsSmall = null;
    @SerializedName("checkboxes_items_per_row_smartphones")
    @Expose
    private List<String> checkboxesItemsPerRowSmartphones = null;
    @SerializedName("checkboxes_items_per_row_iphone5")
    @Expose
    private List<String> checkboxesItemsPerRowIphone5 = null;
    @SerializedName("checkboxes_items_per_row_iphone6")
    @Expose
    private List<String> checkboxesItemsPerRowIphone6 = null;
    @SerializedName("checkboxes_items_per_row_iphone6_plus")
    @Expose
    private List<String> checkboxesItemsPerRowIphone6Plus = null;
    @SerializedName("checkboxes_items_per_row_samsung_galaxy")
    @Expose
    private List<String> checkboxesItemsPerRowSamsungGalaxy = null;
    @SerializedName("checkboxes_items_per_row_tablets_galaxy")
    @Expose
    private List<String> checkboxesItemsPerRowTabletsGalaxy = null;
    @SerializedName("multiple_checkboxes_options_title")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsTitle = null;
    @SerializedName("multiple_checkboxes_options_image")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsImage = null;
    @SerializedName("multiple_checkboxes_options_imagec")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsImagec = null;
    @SerializedName("multiple_checkboxes_options_imagep")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsImagep = null;
    @SerializedName("multiple_checkboxes_options_imagel")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsImagel = null;
    @SerializedName("multiple_checkboxes_options_color")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsColor = null;
    @SerializedName("multiple_checkboxes_options_value")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsValue = null;
    @SerializedName("multiple_checkboxes_options_price")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsPrice = null;
    @SerializedName("multiple_checkboxes_options_sale_price")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsSalePrice = null;
    @SerializedName("multiple_checkboxes_options_price_type")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsPriceType = null;
    @SerializedName("multiple_checkboxes_options_description")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsDescription = null;
    @SerializedName("multiple_checkboxes_options_url")
    @Expose
    private List<List<String>> multipleCheckboxesOptionsUrl = null;
    @SerializedName("checkboxes_uniqid")
    @Expose
    private List<String> checkboxesUniqid = null;
    @SerializedName("checkboxes_clogic")
    @Expose
    private List<String> checkboxesClogic = null;
    @SerializedName("checkboxes_logic")
    @Expose
    private List<String> checkboxesLogic = null;
    @SerializedName("checkboxes_class")
    @Expose
    private List<String> checkboxesClass = null;
    @SerializedName("checkboxes_container_id")
    @Expose
    private List<String> checkboxesContainerId = null;
    @SerializedName("checkboxes_include_tax_for_fee_price_type")
    @Expose
    private List<String> checkboxesIncludeTaxForFeePriceType = null;
    @SerializedName("checkboxes_tax_class_for_fee_price_type")
    @Expose
    private List<String> checkboxesTaxClassForFeePriceType = null;
    @SerializedName("checkboxes_hide_element_label_in_cart")
    @Expose
    private List<String> checkboxesHideElementLabelInCart = null;
    @SerializedName("checkboxes_hide_element_value_in_cart")
    @Expose
    private List<String> checkboxesHideElementValueInCart = null;
    @SerializedName("checkboxes_hide_element_label_in_order")
    @Expose
    private List<String> checkboxesHideElementLabelInOrder = null;
    @SerializedName("checkboxes_hide_element_value_in_order")
    @Expose
    private List<String> checkboxesHideElementValueInOrder = null;
    @SerializedName("checkboxes_hide_element_label_in_floatbox")
    @Expose
    private List<String> checkboxesHideElementLabelInFloatbox = null;
    @SerializedName("checkboxes_hide_element_value_in_floatbox")
    @Expose
    private List<String> checkboxesHideElementValueInFloatbox = null;

    public List<String> getSectionHeaderSize() {
        return sectionHeaderSize;
    }

    public void setSectionHeaderSize(List<String> sectionHeaderSize) {
        this.sectionHeaderSize = sectionHeaderSize;
    }

    public List<String> getSectionDividerType() {
        return sectionDividerType;
    }

    public void setSectionDividerType(List<String> sectionDividerType) {
        this.sectionDividerType = sectionDividerType;
    }

    public List<String> getSections() {
        return sections;
    }

    public void setSections(List<String> sections) {
        this.sections = sections;
    }

    public List<String> getSelectboxInternalName() {
        return selectboxInternalName;
    }

    public void setSelectboxInternalName(List<String> selectboxInternalName) {
        this.selectboxInternalName = selectboxInternalName;
    }

    public List<String> getSelectboxHeaderSize() {
        return selectboxHeaderSize;
    }

    public void setSelectboxHeaderSize(List<String> selectboxHeaderSize) {
        this.selectboxHeaderSize = selectboxHeaderSize;
    }

    public List<String> getSelectboxHeaderTitle() {
        return selectboxHeaderTitle;
    }

    public void setSelectboxHeaderTitle(List<String> selectboxHeaderTitle) {
        this.selectboxHeaderTitle = selectboxHeaderTitle;
    }

    public List<String> getSelectboxHeaderTitlePosition() {
        return selectboxHeaderTitlePosition;
    }

    public void setSelectboxHeaderTitlePosition(List<String> selectboxHeaderTitlePosition) {
        this.selectboxHeaderTitlePosition = selectboxHeaderTitlePosition;
    }

    public List<String> getSelectboxHeaderTitleColor() {
        return selectboxHeaderTitleColor;
    }

    public void setSelectboxHeaderTitleColor(List<String> selectboxHeaderTitleColor) {
        this.selectboxHeaderTitleColor = selectboxHeaderTitleColor;
    }

    public List<String> getSelectboxHeaderSubtitle() {
        return selectboxHeaderSubtitle;
    }

    public void setSelectboxHeaderSubtitle(List<String> selectboxHeaderSubtitle) {
        this.selectboxHeaderSubtitle = selectboxHeaderSubtitle;
    }

    public List<String> getSelectboxHeaderSubtitlePosition() {
        return selectboxHeaderSubtitlePosition;
    }

    public void setSelectboxHeaderSubtitlePosition(List<String> selectboxHeaderSubtitlePosition) {
        this.selectboxHeaderSubtitlePosition = selectboxHeaderSubtitlePosition;
    }

    public List<String> getSelectboxHeaderSubtitleColor() {
        return selectboxHeaderSubtitleColor;
    }

    public void setSelectboxHeaderSubtitleColor(List<String> selectboxHeaderSubtitleColor) {
        this.selectboxHeaderSubtitleColor = selectboxHeaderSubtitleColor;
    }

    public List<String> getSelectboxDividerType() {
        return selectboxDividerType;
    }

    public void setSelectboxDividerType(List<String> selectboxDividerType) {
        this.selectboxDividerType = selectboxDividerType;
    }

    public List<String> getSelectboxEnabled() {
        return selectboxEnabled;
    }

    public void setSelectboxEnabled(List<String> selectboxEnabled) {
        this.selectboxEnabled = selectboxEnabled;
    }

    public List<String> getSelectboxRequired() {
        return selectboxRequired;
    }

    public void setSelectboxRequired(List<String> selectboxRequired) {
        this.selectboxRequired = selectboxRequired;
    }

    public List<String> getSelectboxTextBeforePrice() {
        return selectboxTextBeforePrice;
    }

    public void setSelectboxTextBeforePrice(List<String> selectboxTextBeforePrice) {
        this.selectboxTextBeforePrice = selectboxTextBeforePrice;
    }

    public List<String> getSelectboxTextAfterPrice() {
        return selectboxTextAfterPrice;
    }

    public void setSelectboxTextAfterPrice(List<String> selectboxTextAfterPrice) {
        this.selectboxTextAfterPrice = selectboxTextAfterPrice;
    }

    public List<String> getSelectboxPriceType() {
        return selectboxPriceType;
    }

    public void setSelectboxPriceType(List<String> selectboxPriceType) {
        this.selectboxPriceType = selectboxPriceType;
    }

    public List<String> getSelectboxHideAmount() {
        return selectboxHideAmount;
    }

    public void setSelectboxHideAmount(List<String> selectboxHideAmount) {
        this.selectboxHideAmount = selectboxHideAmount;
    }

    public List<String> getSelectboxQuantity() {
        return selectboxQuantity;
    }

    public void setSelectboxQuantity(List<String> selectboxQuantity) {
        this.selectboxQuantity = selectboxQuantity;
    }

    public List<String> getSelectboxQuantityMin() {
        return selectboxQuantityMin;
    }

    public void setSelectboxQuantityMin(List<String> selectboxQuantityMin) {
        this.selectboxQuantityMin = selectboxQuantityMin;
    }

    public List<String> getSelectboxQuantityMax() {
        return selectboxQuantityMax;
    }

    public void setSelectboxQuantityMax(List<String> selectboxQuantityMax) {
        this.selectboxQuantityMax = selectboxQuantityMax;
    }

    public List<String> getSelectboxQuantityStep() {
        return selectboxQuantityStep;
    }

    public void setSelectboxQuantityStep(List<String> selectboxQuantityStep) {
        this.selectboxQuantityStep = selectboxQuantityStep;
    }

    public List<String> getSelectboxQuantityDefaultValue() {
        return selectboxQuantityDefaultValue;
    }

    public void setSelectboxQuantityDefaultValue(List<String> selectboxQuantityDefaultValue) {
        this.selectboxQuantityDefaultValue = selectboxQuantityDefaultValue;
    }

    public List<String> getSelectboxPlaceholder() {
        return selectboxPlaceholder;
    }

    public void setSelectboxPlaceholder(List<String> selectboxPlaceholder) {
        this.selectboxPlaceholder = selectboxPlaceholder;
    }

    public List<String> getSelectboxUseUrl() {
        return selectboxUseUrl;
    }

    public void setSelectboxUseUrl(List<String> selectboxUseUrl) {
        this.selectboxUseUrl = selectboxUseUrl;
    }

    public List<String> getSelectboxChangesProductImage() {
        return selectboxChangesProductImage;
    }

    public void setSelectboxChangesProductImage(List<String> selectboxChangesProductImage) {
        this.selectboxChangesProductImage = selectboxChangesProductImage;
    }

    public List<List<String>> getMultipleSelectboxOptionsTitle() {
        return multipleSelectboxOptionsTitle;
    }

    public void setMultipleSelectboxOptionsTitle(List<List<String>> multipleSelectboxOptionsTitle) {
        this.multipleSelectboxOptionsTitle = multipleSelectboxOptionsTitle;
    }

    public List<List<String>> getMultipleSelectboxOptionsImage() {
        return multipleSelectboxOptionsImage;
    }

    public void setMultipleSelectboxOptionsImage(List<List<String>> multipleSelectboxOptionsImage) {
        this.multipleSelectboxOptionsImage = multipleSelectboxOptionsImage;
    }

    public List<List<String>> getMultipleSelectboxOptionsImagec() {
        return multipleSelectboxOptionsImagec;
    }

    public void setMultipleSelectboxOptionsImagec(List<List<String>> multipleSelectboxOptionsImagec) {
        this.multipleSelectboxOptionsImagec = multipleSelectboxOptionsImagec;
    }

    public List<List<String>> getMultipleSelectboxOptionsImagep() {
        return multipleSelectboxOptionsImagep;
    }

    public void setMultipleSelectboxOptionsImagep(List<List<String>> multipleSelectboxOptionsImagep) {
        this.multipleSelectboxOptionsImagep = multipleSelectboxOptionsImagep;
    }

    public List<List<String>> getMultipleSelectboxOptionsImagel() {
        return multipleSelectboxOptionsImagel;
    }

    public void setMultipleSelectboxOptionsImagel(List<List<String>> multipleSelectboxOptionsImagel) {
        this.multipleSelectboxOptionsImagel = multipleSelectboxOptionsImagel;
    }

    public List<List<String>> getMultipleSelectboxOptionsValue() {
        return multipleSelectboxOptionsValue;
    }

    public void setMultipleSelectboxOptionsValue(List<List<String>> multipleSelectboxOptionsValue) {
        this.multipleSelectboxOptionsValue = multipleSelectboxOptionsValue;
    }

    public List<List<String>> getMultipleSelectboxOptionsPrice() {
        return multipleSelectboxOptionsPrice;
    }

    public void setMultipleSelectboxOptionsPrice(List<List<String>> multipleSelectboxOptionsPrice) {
        this.multipleSelectboxOptionsPrice = multipleSelectboxOptionsPrice;
    }

    public List<List<String>> getMultipleSelectboxOptionsSalePrice() {
        return multipleSelectboxOptionsSalePrice;
    }

    public void setMultipleSelectboxOptionsSalePrice(List<List<String>> multipleSelectboxOptionsSalePrice) {
        this.multipleSelectboxOptionsSalePrice = multipleSelectboxOptionsSalePrice;
    }

    public List<List<String>> getMultipleSelectboxOptionsPriceType() {
        return multipleSelectboxOptionsPriceType;
    }

    public void setMultipleSelectboxOptionsPriceType(List<List<String>> multipleSelectboxOptionsPriceType) {
        this.multipleSelectboxOptionsPriceType = multipleSelectboxOptionsPriceType;
    }

    public List<List<String>> getMultipleSelectboxOptionsDescription() {
        return multipleSelectboxOptionsDescription;
    }

    public void setMultipleSelectboxOptionsDescription(List<List<String>> multipleSelectboxOptionsDescription) {
        this.multipleSelectboxOptionsDescription = multipleSelectboxOptionsDescription;
    }

    public List<List<String>> getMultipleSelectboxOptionsUrl() {
        return multipleSelectboxOptionsUrl;
    }

    public void setMultipleSelectboxOptionsUrl(List<List<String>> multipleSelectboxOptionsUrl) {
        this.multipleSelectboxOptionsUrl = multipleSelectboxOptionsUrl;
    }

    public List<String> getSelectboxUniqid() {
        return selectboxUniqid;
    }

    public void setSelectboxUniqid(List<String> selectboxUniqid) {
        this.selectboxUniqid = selectboxUniqid;
    }

    public List<String> getSelectboxClogic() {
        return selectboxClogic;
    }

    public void setSelectboxClogic(List<String> selectboxClogic) {
        this.selectboxClogic = selectboxClogic;
    }

    public List<String> getSelectboxLogic() {
        return selectboxLogic;
    }

    public void setSelectboxLogic(List<String> selectboxLogic) {
        this.selectboxLogic = selectboxLogic;
    }

    public List<String> getSelectboxClass() {
        return selectboxClass;
    }

    public void setSelectboxClass(List<String> selectboxClass) {
        this.selectboxClass = selectboxClass;
    }

    public List<String> getSelectboxContainerId() {
        return selectboxContainerId;
    }

    public void setSelectboxContainerId(List<String> selectboxContainerId) {
        this.selectboxContainerId = selectboxContainerId;
    }

    public List<String> getSelectboxIncludeTaxForFeePriceType() {
        return selectboxIncludeTaxForFeePriceType;
    }

    public void setSelectboxIncludeTaxForFeePriceType(List<String> selectboxIncludeTaxForFeePriceType) {
        this.selectboxIncludeTaxForFeePriceType = selectboxIncludeTaxForFeePriceType;
    }

    public List<String> getSelectboxTaxClassForFeePriceType() {
        return selectboxTaxClassForFeePriceType;
    }

    public void setSelectboxTaxClassForFeePriceType(List<String> selectboxTaxClassForFeePriceType) {
        this.selectboxTaxClassForFeePriceType = selectboxTaxClassForFeePriceType;
    }

    public List<String> getSelectboxHideElementLabelInCart() {
        return selectboxHideElementLabelInCart;
    }

    public void setSelectboxHideElementLabelInCart(List<String> selectboxHideElementLabelInCart) {
        this.selectboxHideElementLabelInCart = selectboxHideElementLabelInCart;
    }

    public List<String> getSelectboxHideElementValueInCart() {
        return selectboxHideElementValueInCart;
    }

    public void setSelectboxHideElementValueInCart(List<String> selectboxHideElementValueInCart) {
        this.selectboxHideElementValueInCart = selectboxHideElementValueInCart;
    }

    public List<String> getSelectboxHideElementLabelInOrder() {
        return selectboxHideElementLabelInOrder;
    }

    public void setSelectboxHideElementLabelInOrder(List<String> selectboxHideElementLabelInOrder) {
        this.selectboxHideElementLabelInOrder = selectboxHideElementLabelInOrder;
    }

    public List<String> getSelectboxHideElementValueInOrder() {
        return selectboxHideElementValueInOrder;
    }

    public void setSelectboxHideElementValueInOrder(List<String> selectboxHideElementValueInOrder) {
        this.selectboxHideElementValueInOrder = selectboxHideElementValueInOrder;
    }

    public List<String> getSelectboxHideElementLabelInFloatbox() {
        return selectboxHideElementLabelInFloatbox;
    }

    public void setSelectboxHideElementLabelInFloatbox(List<String> selectboxHideElementLabelInFloatbox) {
        this.selectboxHideElementLabelInFloatbox = selectboxHideElementLabelInFloatbox;
    }

    public List<String> getSelectboxHideElementValueInFloatbox() {
        return selectboxHideElementValueInFloatbox;
    }

    public void setSelectboxHideElementValueInFloatbox(List<String> selectboxHideElementValueInFloatbox) {
        this.selectboxHideElementValueInFloatbox = selectboxHideElementValueInFloatbox;
    }

    public List<String> getCheckboxesInternalName() {
        return checkboxesInternalName;
    }

    public void setCheckboxesInternalName(List<String> checkboxesInternalName) {
        this.checkboxesInternalName = checkboxesInternalName;
    }

    public List<String> getCheckboxesHeaderSize() {
        return checkboxesHeaderSize;
    }

    public void setCheckboxesHeaderSize(List<String> checkboxesHeaderSize) {
        this.checkboxesHeaderSize = checkboxesHeaderSize;
    }

    public List<String> getCheckboxesHeaderTitle() {
        return checkboxesHeaderTitle;
    }

    public void setCheckboxesHeaderTitle(List<String> checkboxesHeaderTitle) {
        this.checkboxesHeaderTitle = checkboxesHeaderTitle;
    }

    public List<String> getCheckboxesHeaderTitlePosition() {
        return checkboxesHeaderTitlePosition;
    }

    public void setCheckboxesHeaderTitlePosition(List<String> checkboxesHeaderTitlePosition) {
        this.checkboxesHeaderTitlePosition = checkboxesHeaderTitlePosition;
    }

    public List<String> getCheckboxesHeaderTitleColor() {
        return checkboxesHeaderTitleColor;
    }

    public void setCheckboxesHeaderTitleColor(List<String> checkboxesHeaderTitleColor) {
        this.checkboxesHeaderTitleColor = checkboxesHeaderTitleColor;
    }

    public List<String> getCheckboxesHeaderSubtitle() {
        return checkboxesHeaderSubtitle;
    }

    public void setCheckboxesHeaderSubtitle(List<String> checkboxesHeaderSubtitle) {
        this.checkboxesHeaderSubtitle = checkboxesHeaderSubtitle;
    }

    public List<String> getCheckboxesHeaderSubtitlePosition() {
        return checkboxesHeaderSubtitlePosition;
    }

    public void setCheckboxesHeaderSubtitlePosition(List<String> checkboxesHeaderSubtitlePosition) {
        this.checkboxesHeaderSubtitlePosition = checkboxesHeaderSubtitlePosition;
    }

    public List<String> getCheckboxesHeaderSubtitleColor() {
        return checkboxesHeaderSubtitleColor;
    }

    public void setCheckboxesHeaderSubtitleColor(List<String> checkboxesHeaderSubtitleColor) {
        this.checkboxesHeaderSubtitleColor = checkboxesHeaderSubtitleColor;
    }

    public List<String> getCheckboxesDividerType() {
        return checkboxesDividerType;
    }

    public void setCheckboxesDividerType(List<String> checkboxesDividerType) {
        this.checkboxesDividerType = checkboxesDividerType;
    }

    public List<String> getCheckboxesEnabled() {
        return checkboxesEnabled;
    }

    public void setCheckboxesEnabled(List<String> checkboxesEnabled) {
        this.checkboxesEnabled = checkboxesEnabled;
    }

    public List<String> getCheckboxesRequired() {
        return checkboxesRequired;
    }

    public void setCheckboxesRequired(List<String> checkboxesRequired) {
        this.checkboxesRequired = checkboxesRequired;
    }

    public List<String> getCheckboxesTextBeforePrice() {
        return checkboxesTextBeforePrice;
    }

    public void setCheckboxesTextBeforePrice(List<String> checkboxesTextBeforePrice) {
        this.checkboxesTextBeforePrice = checkboxesTextBeforePrice;
    }

    public List<String> getCheckboxesTextAfterPrice() {
        return checkboxesTextAfterPrice;
    }

    public void setCheckboxesTextAfterPrice(List<String> checkboxesTextAfterPrice) {
        this.checkboxesTextAfterPrice = checkboxesTextAfterPrice;
    }

    public List<String> getCheckboxesHideAmount() {
        return checkboxesHideAmount;
    }

    public void setCheckboxesHideAmount(List<String> checkboxesHideAmount) {
        this.checkboxesHideAmount = checkboxesHideAmount;
    }

    public List<String> getCheckboxesQuantity() {
        return checkboxesQuantity;
    }

    public void setCheckboxesQuantity(List<String> checkboxesQuantity) {
        this.checkboxesQuantity = checkboxesQuantity;
    }

    public List<String> getCheckboxesQuantityMin() {
        return checkboxesQuantityMin;
    }

    public void setCheckboxesQuantityMin(List<String> checkboxesQuantityMin) {
        this.checkboxesQuantityMin = checkboxesQuantityMin;
    }

    public List<String> getCheckboxesQuantityMax() {
        return checkboxesQuantityMax;
    }

    public void setCheckboxesQuantityMax(List<String> checkboxesQuantityMax) {
        this.checkboxesQuantityMax = checkboxesQuantityMax;
    }

    public List<String> getCheckboxesQuantityStep() {
        return checkboxesQuantityStep;
    }

    public void setCheckboxesQuantityStep(List<String> checkboxesQuantityStep) {
        this.checkboxesQuantityStep = checkboxesQuantityStep;
    }

    public List<String> getCheckboxesQuantityDefaultValue() {
        return checkboxesQuantityDefaultValue;
    }

    public void setCheckboxesQuantityDefaultValue(List<String> checkboxesQuantityDefaultValue) {
        this.checkboxesQuantityDefaultValue = checkboxesQuantityDefaultValue;
    }

    public List<String> getCheckboxesLimitChoices() {
        return checkboxesLimitChoices;
    }

    public void setCheckboxesLimitChoices(List<String> checkboxesLimitChoices) {
        this.checkboxesLimitChoices = checkboxesLimitChoices;
    }

    public List<String> getCheckboxesExactlimitChoices() {
        return checkboxesExactlimitChoices;
    }

    public void setCheckboxesExactlimitChoices(List<String> checkboxesExactlimitChoices) {
        this.checkboxesExactlimitChoices = checkboxesExactlimitChoices;
    }

    public List<String> getCheckboxesMinimumlimitChoices() {
        return checkboxesMinimumlimitChoices;
    }

    public void setCheckboxesMinimumlimitChoices(List<String> checkboxesMinimumlimitChoices) {
        this.checkboxesMinimumlimitChoices = checkboxesMinimumlimitChoices;
    }

    public List<String> getCheckboxesUseImages() {
        return checkboxesUseImages;
    }

    public void setCheckboxesUseImages(List<String> checkboxesUseImages) {
        this.checkboxesUseImages = checkboxesUseImages;
    }

    public List<String> getCheckboxesUseLightbox() {
        return checkboxesUseLightbox;
    }

    public void setCheckboxesUseLightbox(List<String> checkboxesUseLightbox) {
        this.checkboxesUseLightbox = checkboxesUseLightbox;
    }

    public List<String> getCheckboxesSwatchmode() {
        return checkboxesSwatchmode;
    }

    public void setCheckboxesSwatchmode(List<String> checkboxesSwatchmode) {
        this.checkboxesSwatchmode = checkboxesSwatchmode;
    }

    public List<String> getCheckboxesUseColors() {
        return checkboxesUseColors;
    }

    public void setCheckboxesUseColors(List<String> checkboxesUseColors) {
        this.checkboxesUseColors = checkboxesUseColors;
    }

    public List<String> getCheckboxesChangesProductImage() {
        return checkboxesChangesProductImage;
    }

    public void setCheckboxesChangesProductImage(List<String> checkboxesChangesProductImage) {
        this.checkboxesChangesProductImage = checkboxesChangesProductImage;
    }

    public List<String> getCheckboxesItemsPerRow() {
        return checkboxesItemsPerRow;
    }

    public void setCheckboxesItemsPerRow(List<String> checkboxesItemsPerRow) {
        this.checkboxesItemsPerRow = checkboxesItemsPerRow;
    }

    public List<String> getCheckboxesItemsPerRowTablets() {
        return checkboxesItemsPerRowTablets;
    }

    public void setCheckboxesItemsPerRowTablets(List<String> checkboxesItemsPerRowTablets) {
        this.checkboxesItemsPerRowTablets = checkboxesItemsPerRowTablets;
    }

    public List<String> getCheckboxesItemsPerRowTabletsSmall() {
        return checkboxesItemsPerRowTabletsSmall;
    }

    public void setCheckboxesItemsPerRowTabletsSmall(List<String> checkboxesItemsPerRowTabletsSmall) {
        this.checkboxesItemsPerRowTabletsSmall = checkboxesItemsPerRowTabletsSmall;
    }

    public List<String> getCheckboxesItemsPerRowSmartphones() {
        return checkboxesItemsPerRowSmartphones;
    }

    public void setCheckboxesItemsPerRowSmartphones(List<String> checkboxesItemsPerRowSmartphones) {
        this.checkboxesItemsPerRowSmartphones = checkboxesItemsPerRowSmartphones;
    }

    public List<String> getCheckboxesItemsPerRowIphone5() {
        return checkboxesItemsPerRowIphone5;
    }

    public void setCheckboxesItemsPerRowIphone5(List<String> checkboxesItemsPerRowIphone5) {
        this.checkboxesItemsPerRowIphone5 = checkboxesItemsPerRowIphone5;
    }

    public List<String> getCheckboxesItemsPerRowIphone6() {
        return checkboxesItemsPerRowIphone6;
    }

    public void setCheckboxesItemsPerRowIphone6(List<String> checkboxesItemsPerRowIphone6) {
        this.checkboxesItemsPerRowIphone6 = checkboxesItemsPerRowIphone6;
    }

    public List<String> getCheckboxesItemsPerRowIphone6Plus() {
        return checkboxesItemsPerRowIphone6Plus;
    }

    public void setCheckboxesItemsPerRowIphone6Plus(List<String> checkboxesItemsPerRowIphone6Plus) {
        this.checkboxesItemsPerRowIphone6Plus = checkboxesItemsPerRowIphone6Plus;
    }

    public List<String> getCheckboxesItemsPerRowSamsungGalaxy() {
        return checkboxesItemsPerRowSamsungGalaxy;
    }

    public void setCheckboxesItemsPerRowSamsungGalaxy(List<String> checkboxesItemsPerRowSamsungGalaxy) {
        this.checkboxesItemsPerRowSamsungGalaxy = checkboxesItemsPerRowSamsungGalaxy;
    }

    public List<String> getCheckboxesItemsPerRowTabletsGalaxy() {
        return checkboxesItemsPerRowTabletsGalaxy;
    }

    public void setCheckboxesItemsPerRowTabletsGalaxy(List<String> checkboxesItemsPerRowTabletsGalaxy) {
        this.checkboxesItemsPerRowTabletsGalaxy = checkboxesItemsPerRowTabletsGalaxy;
    }

    public List<List<String>> getMultipleCheckboxesOptionsTitle() {
        return multipleCheckboxesOptionsTitle;
    }

    public void setMultipleCheckboxesOptionsTitle(List<List<String>> multipleCheckboxesOptionsTitle) {
        this.multipleCheckboxesOptionsTitle = multipleCheckboxesOptionsTitle;
    }

    public List<List<String>> getMultipleCheckboxesOptionsImage() {
        return multipleCheckboxesOptionsImage;
    }

    public void setMultipleCheckboxesOptionsImage(List<List<String>> multipleCheckboxesOptionsImage) {
        this.multipleCheckboxesOptionsImage = multipleCheckboxesOptionsImage;
    }

    public List<List<String>> getMultipleCheckboxesOptionsImagec() {
        return multipleCheckboxesOptionsImagec;
    }

    public void setMultipleCheckboxesOptionsImagec(List<List<String>> multipleCheckboxesOptionsImagec) {
        this.multipleCheckboxesOptionsImagec = multipleCheckboxesOptionsImagec;
    }

    public List<List<String>> getMultipleCheckboxesOptionsImagep() {
        return multipleCheckboxesOptionsImagep;
    }

    public void setMultipleCheckboxesOptionsImagep(List<List<String>> multipleCheckboxesOptionsImagep) {
        this.multipleCheckboxesOptionsImagep = multipleCheckboxesOptionsImagep;
    }

    public List<List<String>> getMultipleCheckboxesOptionsImagel() {
        return multipleCheckboxesOptionsImagel;
    }

    public void setMultipleCheckboxesOptionsImagel(List<List<String>> multipleCheckboxesOptionsImagel) {
        this.multipleCheckboxesOptionsImagel = multipleCheckboxesOptionsImagel;
    }

    public List<List<String>> getMultipleCheckboxesOptionsColor() {
        return multipleCheckboxesOptionsColor;
    }

    public void setMultipleCheckboxesOptionsColor(List<List<String>> multipleCheckboxesOptionsColor) {
        this.multipleCheckboxesOptionsColor = multipleCheckboxesOptionsColor;
    }

    public List<List<String>> getMultipleCheckboxesOptionsValue() {
        return multipleCheckboxesOptionsValue;
    }

    public void setMultipleCheckboxesOptionsValue(List<List<String>> multipleCheckboxesOptionsValue) {
        this.multipleCheckboxesOptionsValue = multipleCheckboxesOptionsValue;
    }

    public List<List<String>> getMultipleCheckboxesOptionsPrice() {
        return multipleCheckboxesOptionsPrice;
    }

    public void setMultipleCheckboxesOptionsPrice(List<List<String>> multipleCheckboxesOptionsPrice) {
        this.multipleCheckboxesOptionsPrice = multipleCheckboxesOptionsPrice;
    }

    public List<List<String>> getMultipleCheckboxesOptionsSalePrice() {
        return multipleCheckboxesOptionsSalePrice;
    }

    public void setMultipleCheckboxesOptionsSalePrice(List<List<String>> multipleCheckboxesOptionsSalePrice) {
        this.multipleCheckboxesOptionsSalePrice = multipleCheckboxesOptionsSalePrice;
    }

    public List<List<String>> getMultipleCheckboxesOptionsPriceType() {
        return multipleCheckboxesOptionsPriceType;
    }

    public void setMultipleCheckboxesOptionsPriceType(List<List<String>> multipleCheckboxesOptionsPriceType) {
        this.multipleCheckboxesOptionsPriceType = multipleCheckboxesOptionsPriceType;
    }

    public List<List<String>> getMultipleCheckboxesOptionsDescription() {
        return multipleCheckboxesOptionsDescription;
    }

    public void setMultipleCheckboxesOptionsDescription(List<List<String>> multipleCheckboxesOptionsDescription) {
        this.multipleCheckboxesOptionsDescription = multipleCheckboxesOptionsDescription;
    }

    public List<List<String>> getMultipleCheckboxesOptionsUrl() {
        return multipleCheckboxesOptionsUrl;
    }

    public void setMultipleCheckboxesOptionsUrl(List<List<String>> multipleCheckboxesOptionsUrl) {
        this.multipleCheckboxesOptionsUrl = multipleCheckboxesOptionsUrl;
    }

    public List<String> getCheckboxesUniqid() {
        return checkboxesUniqid;
    }

    public void setCheckboxesUniqid(List<String> checkboxesUniqid) {
        this.checkboxesUniqid = checkboxesUniqid;
    }

    public List<String> getCheckboxesClogic() {
        return checkboxesClogic;
    }

    public void setCheckboxesClogic(List<String> checkboxesClogic) {
        this.checkboxesClogic = checkboxesClogic;
    }

    public List<String> getCheckboxesLogic() {
        return checkboxesLogic;
    }

    public void setCheckboxesLogic(List<String> checkboxesLogic) {
        this.checkboxesLogic = checkboxesLogic;
    }

    public List<String> getCheckboxesClass() {
        return checkboxesClass;
    }

    public void setCheckboxesClass(List<String> checkboxesClass) {
        this.checkboxesClass = checkboxesClass;
    }

    public List<String> getCheckboxesContainerId() {
        return checkboxesContainerId;
    }

    public void setCheckboxesContainerId(List<String> checkboxesContainerId) {
        this.checkboxesContainerId = checkboxesContainerId;
    }

    public List<String> getCheckboxesIncludeTaxForFeePriceType() {
        return checkboxesIncludeTaxForFeePriceType;
    }

    public void setCheckboxesIncludeTaxForFeePriceType(List<String> checkboxesIncludeTaxForFeePriceType) {
        this.checkboxesIncludeTaxForFeePriceType = checkboxesIncludeTaxForFeePriceType;
    }

    public List<String> getCheckboxesTaxClassForFeePriceType() {
        return checkboxesTaxClassForFeePriceType;
    }

    public void setCheckboxesTaxClassForFeePriceType(List<String> checkboxesTaxClassForFeePriceType) {
        this.checkboxesTaxClassForFeePriceType = checkboxesTaxClassForFeePriceType;
    }

    public List<String> getCheckboxesHideElementLabelInCart() {
        return checkboxesHideElementLabelInCart;
    }

    public void setCheckboxesHideElementLabelInCart(List<String> checkboxesHideElementLabelInCart) {
        this.checkboxesHideElementLabelInCart = checkboxesHideElementLabelInCart;
    }

    public List<String> getCheckboxesHideElementValueInCart() {
        return checkboxesHideElementValueInCart;
    }

    public void setCheckboxesHideElementValueInCart(List<String> checkboxesHideElementValueInCart) {
        this.checkboxesHideElementValueInCart = checkboxesHideElementValueInCart;
    }

    public List<String> getCheckboxesHideElementLabelInOrder() {
        return checkboxesHideElementLabelInOrder;
    }

    public void setCheckboxesHideElementLabelInOrder(List<String> checkboxesHideElementLabelInOrder) {
        this.checkboxesHideElementLabelInOrder = checkboxesHideElementLabelInOrder;
    }

    public List<String> getCheckboxesHideElementValueInOrder() {
        return checkboxesHideElementValueInOrder;
    }

    public void setCheckboxesHideElementValueInOrder(List<String> checkboxesHideElementValueInOrder) {
        this.checkboxesHideElementValueInOrder = checkboxesHideElementValueInOrder;
    }

    public List<String> getCheckboxesHideElementLabelInFloatbox() {
        return checkboxesHideElementLabelInFloatbox;
    }

    public void setCheckboxesHideElementLabelInFloatbox(List<String> checkboxesHideElementLabelInFloatbox) {
        this.checkboxesHideElementLabelInFloatbox = checkboxesHideElementLabelInFloatbox;
    }

    public List<String> getCheckboxesHideElementValueInFloatbox() {
        return checkboxesHideElementValueInFloatbox;
    }

    public void setCheckboxesHideElementValueInFloatbox(List<String> checkboxesHideElementValueInFloatbox) {
        this.checkboxesHideElementValueInFloatbox = checkboxesHideElementValueInFloatbox;
    }

}
