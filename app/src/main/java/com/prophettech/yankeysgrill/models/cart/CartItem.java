/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.models.cart;

import java.util.Map;

public class CartItem {

    private double productId;
    private double variationIds;
    private int quantity;
    private String name;
    private double price;
    private double originalPrice;
    private double optionsPrice;
    private Map<String, String> options;

    public double getOptionsPrice() {
        return optionsPrice;
    }

    public void setOptionsPrice(double optionsPrice) {
        this.optionsPrice = optionsPrice;
    }

    public Map<String, String> getOptions() {
        return options;
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public double getProductId() {
        return productId;
    }

    public void setProductId(double productId) {
        this.productId = productId;
    }

    public double getVariationIds() {
        return variationIds;
    }

    public void setVariationIds(double variationIds) {
        this.variationIds = variationIds;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
