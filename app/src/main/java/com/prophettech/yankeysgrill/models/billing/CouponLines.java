/*
 * *
 *  * Created by Mohammed Akram Hussain on 22/12/20 17:08
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/12/20 17:07
 *
 */

package com.prophettech.yankeysgrill.models.billing;

import com.google.gson.annotations.SerializedName;

public class CouponLines {

    @SerializedName("code")
    private String couponCode;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}
