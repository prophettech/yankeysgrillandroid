/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.models.retrofit;

public class NotesResponse {
    private float id;
    private String author;
    private String date_created;
    private String date_created_gmt;
    private String note;
    private boolean customer_note;

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_created_gmt() {
        return date_created_gmt;
    }

    public void setDate_created_gmt(String date_created_gmt) {
        this.date_created_gmt = date_created_gmt;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isCustomer_note() {
        return customer_note;
    }

    public void setCustomer_note(boolean customer_note) {
        this.customer_note = customer_note;
    }
}
