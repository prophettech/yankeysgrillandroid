/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.models.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;

public class ArrayObjectDualityDeserializer implements JsonDeserializer<MetaDatum> {

    public static final String TAG = ArrayObjectDualityDeserializer.class.getSimpleName();

    @Override
    public MetaDatum deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        MetaDatum response = new MetaDatum();
        JsonObject object = json.getAsJsonObject();
        if (object.get("value").isJsonObject()) {
            Gson gson = new Gson();
            try {
                Value dtoObject = gson.fromJson(object.get("value"), Value.class);
                response.setValue(dtoObject);
            } catch (JsonSyntaxException e) {
                Log.e(TAG, "Error " + e);
            }
        } else if (object.get("value").isJsonArray()) {

        } else if (object.get("value").isJsonNull()) {

        } else {
            //response.setValueStr(object.get("value").getAsString());
        }
        return response;
    }
}
