/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.yankeysgrill.base;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.activities.CartActivity;
import com.prophettech.yankeysgrill.customview.TransparentLoadAnimation;
import com.prophettech.yankeysgrill.threading.MainThread;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements IBaseView {

    public static final String TAG = BaseActivity.class.getSimpleName();
    private static MainThread mMainUIThread;
    protected IBaseView mBaseView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.cartView)
    ImageButton mCartButton;
    private TransparentLoadAnimation mLoadAnimation;

    protected final void onCreate(Bundle savedInstanceState, int resourceLayout) {
        super.onCreate(savedInstanceState);
        mMainUIThread = MainThread.getInstance();

        setContentView(resourceLayout);
        ButterKnife.bind(this);
        initProgress();
        setUpToolBar();

        mCartButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        });
    }

    private void initProgress() {
        mLoadAnimation = new TransparentLoadAnimation(this);
    }

    protected void setUpToolBar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mToolbar.setTitleTextColor(Color.WHITE);
        }
        onToolBarSetUp(mToolbar, getSupportActionBar());
    }

    @Override
    public void showProgress(boolean show) {
        mMainUIThread.post(() -> {
            if (show) {
                mLoadAnimation.showProgressView();
            } else {
                mLoadAnimation.hideProgressView();
            }
        });
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public abstract void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar);

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public IBaseView getBaseView() {
        return mBaseView;
    }
}
