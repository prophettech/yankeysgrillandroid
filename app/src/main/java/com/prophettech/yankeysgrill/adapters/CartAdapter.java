/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.yankeysgrill.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.models.cart.CartItem;
import com.prophettech.yankeysgrill.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    private static final String TAG = CartAdapter.class.getName();

    private List<CartItem> mCartItems;
    private CartInterface mCartInterface;
    private boolean mHideQuantityLayout;

    public CartAdapter(CartInterface cartInterface, boolean hide) {
        mCartInterface = cartInterface;
        mHideQuantityLayout = hide;
        mCartItems = new ArrayList<>();
    }

    public void refresh(List<CartItem> cartItems) {
        mCartItems = cartItems;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cart_item, parent, false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        CartItem item = mCartItems.get(position);
        holder.mTitle.setText(item.getName());
        holder.mId.setText("# " + item.getProductId());
        holder.mQuantity.setText(String.valueOf(item.getQuantity()));
        holder.mCount.setText(String.valueOf(item.getQuantity()));
        holder.mPrice.setText("￡" + CommonUtils.getTwoDecimal(item.getPrice()));
        holder.mOriginalPrice.setText("￡" + CommonUtils.getTwoDecimal(Double.parseDouble(String.valueOf(item.getOriginalPrice()))));
        holder.mOptionsAmount.setText("￡" + CommonUtils.getTwoDecimal(Double.parseDouble(String.valueOf(item.getOptionsPrice()))));
        if (mHideQuantityLayout) {
            holder.mQuantityLayout.setVisibility(View.GONE);
        }
        if (item.getOptions() != null && !item.getOptions().isEmpty()) {
            List<String> optionsTitle = new ArrayList<>(item.getOptions().keySet());
            List<String> optionsPrice = new ArrayList<>(item.getOptions().values());
            StringBuilder stringBuilder = new StringBuilder();
            for (String option : optionsTitle) {
                try {
                    stringBuilder.append(option + " ￡" + CommonUtils.getTwoDecimal(Double.parseDouble(item.getOptions().get(option))) + "\n");
                } catch (NumberFormatException e) {
                    Log.d(TAG, Objects.requireNonNull(e.getLocalizedMessage()));
                }
            }
            holder.mId.setText(stringBuilder);
        }
        holder.setUpListeners(item, position);
    }

    @Override
    public int getItemCount() {
        return mCartItems.size();
    }

    public interface CartInterface {
        void onAddClick(CartItem cartItem, int position);

        void onRemoveClick(CartItem cartItem, int position);
    }

    class CartViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.titleTV)
        TextView mTitle;
        @BindView(R.id.idTV)
        TextView mId;
        @BindView(R.id.quantityTV)
        TextView mQuantity;
        @BindView(R.id.priceTV)
        TextView mPrice;
        @BindView(R.id.ivAdd)
        ImageView mAdd;
        @BindView(R.id.ivRemove)
        ImageView mRemove;
        @BindView(R.id.tvCount)
        TextView mCount;
        @BindView(R.id.quantityLayout)
        LinearLayout mQuantityLayout;
        @BindView(R.id.originalPriceTV)
        TextView mOriginalPrice;
        @BindView(R.id.optionsTV)
        TextView mOptionsAmount;

        CartViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        private void setUpListeners(CartItem cartItem, int position) {
            mAdd.setOnClickListener(v -> mCartInterface.onAddClick(cartItem, position));

            mRemove.setOnClickListener(v -> mCartInterface.onRemoveClick(cartItem, position));
        }

    }
}
