/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.GlidePalette;
import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.models.CategoriesModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    private List<CategoriesModel> mCategoriesList;
    private CategoryClickListener mCategoryClickListener;
    private Context mContext;

    public CategoriesAdapter() {
        mCategoriesList = new ArrayList<>();
    }

    public void refresh(List<CategoriesModel> list, CategoryClickListener clickListener) {
        mCategoriesList = list;
        mCategoryClickListener = clickListener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_list_item, parent, false);
        mContext = parent.getContext();
        return new CategoriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesViewHolder holder, int position) {
        CategoriesModel item = mCategoriesList.get(position);
        holder.mCategoryName.setText(item.getName());
        holder.mCategoryDescription.setText(Html.fromHtml(item.getDescription()).toString());
        Glide.with(mContext)
                .load(item.getImage().getSrc())
                .listener(GlidePalette.with(item.getImage().getSrc())
                        .intoCallBack(palette -> {
                            Palette.Swatch swatch = palette.getVibrantSwatch();
                            if (swatch != null) {
                                holder.mCategoryCardView.setBackgroundColor(swatch.getRgb());
                                holder.mCategoryName.setTextColor(swatch.getBodyTextColor());
                                holder.mCategoryDescription.setTextColor(swatch.getBodyTextColor());
                            }
                        })).into(holder.mCategoryImage);
        holder.mCategoryCardView.setOnClickListener(v -> mCategoryClickListener.onCategoryClick(item.getId()));
    }

    @Override
    public int getItemCount() {
        return mCategoriesList.size();
    }

    public interface CategoryClickListener {
        void onCategoryClick(double categoryId);
    }

    static class CategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoryImageView)
        ImageView mCategoryImage;
        @BindView(R.id.categoryName)
        TextView mCategoryName;
        @BindView(R.id.categoryDescription)
        TextView mCategoryDescription;
        @BindView(R.id.categoryCardView)
        CardView mCategoryCardView;

        CategoriesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
