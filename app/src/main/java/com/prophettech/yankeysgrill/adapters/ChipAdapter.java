/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.adapters;

import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChipAdapter extends RecyclerView.Adapter<ChipAdapter.ChipViewHolder> {

    private Map<String, String> optionsMap;
    private IChipInterface mChipInterface;

    public ChipAdapter(IChipInterface listener) {
        this.mChipInterface = listener;
        optionsMap = new ArrayMap<>();
    }

    public void refresh(Map<String, String> list) {
        optionsMap = list;
        notifyDataSetChanged();
    }

    public void removeChip(String key) {
        optionsMap.remove(key);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ChipViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_chip_view, parent, false);
        return new ChipViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChipViewHolder holder, int position) {
        List<String> optionsTitle = new ArrayList<>(optionsMap.keySet());
        List<String> optionsPrice = new ArrayList<>(optionsMap.values());
        if (TextUtils.isEmpty(optionsPrice.get(position))){
            holder.mTitle.setText(optionsTitle.get(position) + " ￡" + "0");
        }else {
            holder.mTitle.setText(optionsTitle.get(position) + " ￡" + CommonUtils.getTwoDecimal(Double.parseDouble(optionsPrice.get(position))));
        }
        holder.setListeners(optionsTitle.get(position), position);
    }

    @Override
    public int getItemCount() {
        return optionsMap.size();
    }

    public interface IChipInterface {
        void onCloseClick(String key, int position);

        void onChipClick(String key, int position);
    }

    class ChipViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivClose)
        ImageView mClose;
        @BindView(R.id.tvTitle)
        TextView mTitle;

        ChipViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setListeners(final String key, int position) {
            mClose.setOnClickListener(v -> {
                if (mChipInterface != null) {
                    mChipInterface.onCloseClick(key, position);
                }
            });

            mTitle.setOnClickListener(v -> {
                if (mChipInterface != null) {
                    mChipInterface.onChipClick(key, position);
                }
            });
        }

    }
}
