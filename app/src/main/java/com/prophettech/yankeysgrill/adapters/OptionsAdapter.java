/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.adapters;

import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.yankeysgrill.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.OptionsViewHolder> {

    private Map<String, String> optionsMap;
    private IOptionSelected mListener;

    public OptionsAdapter(IOptionSelected listener) {
        mListener = listener;
        optionsMap = new ArrayMap<>();
    }

    public void refresh(Map<String, String> list) {
        optionsMap = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OptionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_options_item, parent, false);
        return new OptionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OptionsViewHolder holder, int position) {
        List<String> optionsTitle = new ArrayList<>(optionsMap.keySet());
        List<String> optionsPrice = new ArrayList<>(optionsMap.values());
        holder.cbOption.setText(optionsTitle.get(position));
        holder.tvOptionPrice.setText("￡" + optionsPrice.get(position));
        holder.setUpListener(optionsTitle.get(position), optionsPrice.get(position));
    }

    @Override
    public int getItemCount() {
        return optionsMap.size();
    }

    public interface IOptionSelected {
        void onCheckboxClicked(String key, String value, boolean isChecked);
    }

    class OptionsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cbOption)
        CheckBox cbOption;
        @BindView(R.id.tvOptionPrice)
        TextView tvOptionPrice;

        OptionsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setUpListener(String key, String value) {
            cbOption.setOnCheckedChangeListener((buttonView, isChecked) -> mListener.onCheckboxClicked(key, value, isChecked));
        }
    }
}
