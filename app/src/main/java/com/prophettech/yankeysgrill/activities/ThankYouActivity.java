/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.yankeysgrill.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.base.BaseActivity;
import com.prophettech.yankeysgrill.models.billing.OrderPlacedResponse;

import butterknife.BindView;

public class ThankYouActivity extends BaseActivity {

    @BindView(R.id.tvOrderId)
    TextView mOrderId;
    @BindView(R.id.tvDate)
    TextView mDate;
    @BindView(R.id.tvEmail)
    TextView mEmail;
    @BindView(R.id.tvPhone)
    TextView mPhone;
    @BindView(R.id.tvAddress)
    TextView mAddress;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_thank_you);
        OrderPlacedResponse data = (OrderPlacedResponse) getIntent().getSerializableExtra("orderDetails");
        if (data != null) {
            mOrderId.setText("#" + data.getId());
            mDate.setText(data.getDateCreated());
            mEmail.setText(data.getBilling().getEmail());
            mPhone.setText(data.getBilling().getPhone());
            StringBuilder address = new StringBuilder();
            address.append(data.getBilling().getAddress1());
            address.append("\n");
            address.append(data.getBilling().getAddress2());
            address.append("\n");
            address.append(data.getBilling().getCity());
            address.append("\n");
            address.append(data.getBilling().getPostCode());
            address.append("\n");
            address.append(data.getBilling().getCountry());
            mAddress.setText(address);
        }
    }

    public void onDoneClick(View view) {
        Intent intent = new Intent(this, CategoriesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar) {
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        TextView textView = toolbar.findViewById(R.id.tvHeading);
        textView.setText(R.string.thank_you);
        ImageButton cartButton = toolbar.findViewById(R.id.cartView);
        cartButton.setVisibility(View.GONE);
    }
}
