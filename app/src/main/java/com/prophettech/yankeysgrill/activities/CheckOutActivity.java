/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:20
 *
 */

package com.prophettech.yankeysgrill.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.adapters.CartAdapter;
import com.prophettech.yankeysgrill.base.BaseActivity;
import com.prophettech.yankeysgrill.models.billing.BillingModel;
import com.prophettech.yankeysgrill.models.billing.CouponLines;
import com.prophettech.yankeysgrill.models.billing.LineItems;
import com.prophettech.yankeysgrill.models.billing.Order;
import com.prophettech.yankeysgrill.models.billing.OrderPlacedResponse;
import com.prophettech.yankeysgrill.models.billing.ShippingLines;
import com.prophettech.yankeysgrill.models.cart.Cart;
import com.prophettech.yankeysgrill.models.cart.CartItem;
import com.prophettech.yankeysgrill.models.retrofit.APIInterface;
import com.prophettech.yankeysgrill.models.retrofit.NotesBody;
import com.prophettech.yankeysgrill.models.retrofit.NotesResponse;
import com.prophettech.yankeysgrill.models.retrofit.RetrofitClientInstance;
import com.prophettech.yankeysgrill.rest.Services;
import com.prophettech.yankeysgrill.stripe.PaymentResultCallback;
import com.prophettech.yankeysgrill.stripe.StripeBackendApi;
import com.prophettech.yankeysgrill.stripe.StripeRetrofitFactory;
import com.prophettech.yankeysgrill.utils.Analytics;
import com.prophettech.yankeysgrill.utils.CommonUtils;
import com.prophettech.yankeysgrill.utils.OverlayDialog;
import com.prophettech.yankeysgrill.utils.PaperUtil;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.model.Address;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutActivity extends BaseActivity implements CartAdapter.CartInterface {

    @BindView(R.id.tvName)
    EditText etFirstName;
    @BindView(R.id.tvEmail)
    EditText etLastName;
    @BindView(R.id.tvAddress)
    EditText etAddress;
    @BindView(R.id.tvAddress1)
    EditText etAddress2;
    @BindView(R.id.tvTown1)
    EditText etTown;
    @BindView(R.id.etPostCode)
    EditText etPostCode;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.emailET)
    EditText etEmail;
    @BindView(R.id.etOrderNotes)
    EditText etOrderNotes;
    @BindView(R.id.rvCart)
    RecyclerView mRecyclerView;
    @BindView(R.id.tvTotalQuantity)
    TextView mTotalQuantity;
    @BindView(R.id.tvTotalPrice)
    TextView mTotalPrice;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.radioGroup1)
    RadioGroup mShippingMethod;
    @BindView(R.id.cardInputWidget)
    CardInputWidget mCardInputWidget;
    @BindView(R.id.tvDiscount)
    TextView mDiscount;
    @BindView(R.id.tvTotal)
    TextView mFinalTotal;
    private int mTotal = 0;
    private boolean mCash = false;
    private boolean mDelivery = false;
    private Stripe stripe;
    private String paymentIntentClientSecret;
    private boolean mValidPostCode = false;
    private String mDeliveryCharge = "2.0";
    private boolean mPostCodeExist = false;
    private double totalPrice = 0;
    private OverlayDialog overlayDialog;
    private double discountPrice = 0;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_check_out);
        setData();
        initViews();
    }

    private void setData() {
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        CartAdapter mAdapter = new CartAdapter(this, true);
        mRecyclerView.setAdapter(mAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        Cart items = PaperUtil.getCartItems();
        if (items != null) {
            mAdapter.refresh(items.getProducts());
            mTotalQuantity.setText(String.valueOf(items.getTotalQuantity()));
            mTotal = (int) (items.getTotalPrice() * 100);
            totalPrice = items.getTotalPrice();
            discountPrice = calculateDiscount(items.getTotalPrice());
            mDiscount.setText("- ￡" + CommonUtils.getTwoDecimal(totalPrice - discountPrice));
            mTotalPrice.setText("￡" + CommonUtils.getTwoDecimal(items.getTotalPrice()));
            mFinalTotal.setText("￡" + CommonUtils.getTwoDecimal(discountPrice));        }
    }

    private double calculateDiscount(double totalPrice){
        return  (90*totalPrice)/100;
    }

    private void initViews() {
        mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton paymentType = findViewById(checkedId);
            if (TextUtils.equals(paymentType.getText(), getString(R.string.pay_by_cash))) {
                mCash = true;
                setUpStripeInput();
            } else if (TextUtils.equals(paymentType.getText(), getString(R.string.pay_via_stripe))) {
                mCash = false;
                setUpStripeInput();
            }
        });
        mCardInputWidget.setVisibility(View.GONE);
        mShippingMethod.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton shippingMethod = findViewById(checkedId);
            if (TextUtils.equals(shippingMethod.getText(), "Collection")) {
                mDelivery = false;
                updateDeliveryCharge();
            } else if (TextUtils.equals(shippingMethod.getText(), "Delivery")) {
                mDelivery = true;
                updateDeliveryCharge();
            }
        });

        etPostCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (CommonUtils.isValidPostCode(s.toString())) {
                    mValidPostCode = true;
                    calculatePostCodeCharges(s.toString());
                    updateDeliveryCharge();
                } else {
                    mValidPostCode = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setUpStripeInput() {
        if (mCash) {
            mCardInputWidget.setVisibility(View.GONE);
        } else {
            mCardInputWidget.setVisibility(View.VISIBLE);
        }
    }

    private void updateDeliveryCharge() {
        if (mDelivery) {
            mTotalPrice.setText("￡" + CommonUtils.getTwoDecimal(totalPrice) + " + ￡ " + mDeliveryCharge + " Delivery");
        } else {
            mTotalPrice.setText("￡" + CommonUtils.getTwoDecimal(totalPrice));
        }
    }

    public void placeOrder(View view) {
        if (!CommonUtils.isValidPostCode(etPostCode.getText().toString()) || !mPostCodeExist) {
            overlayDialog = new OverlayDialog(this,
                    v -> {
                        overlayDialog.dismissDialog();
                        proceedToPlaceOrder();
                    },
                    v -> {
                        overlayDialog.dismissDialog();
                    },
                    "Postcode Outside Delivery Zone",
                    "Postcode is either incorrect or outside delivery zone. Do you want to continue ?",
                    "Proceed",
                    "Cancel");
            overlayDialog.showDialog();
        } else {
            proceedToPlaceOrder();
        }
    }

    private void proceedToPlaceOrder() {
        if (isFormValidated()) {
            if (mCash) {
                placeOrderOnWordPress();
            } else {
                startCheckoutViaStripe();
            }
        }
    }

    private void startCheckoutViaStripe() {
        showProgress(true);
        StripeBackendApi service = StripeRetrofitFactory.getRetrofitInstance().create(StripeBackendApi.class);

        int finalTotal;
        if (mDelivery) {
            finalTotal = (int) ((totalPrice + Double.parseDouble(mDeliveryCharge)) * 100);
        }else {
            finalTotal = (int) ((totalPrice ) * 100);
        }
        Call<ResponseBody> call = service.createPaymentIntent(finalTotal, "gbp");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {
                        final String body = response.body().string();
                        JSONObject clientObject = new JSONObject(body);
                        paymentIntentClientSecret = clientObject.getString("clientSecret");
                        confirmStripePayment();
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                Analytics.log(CheckOutActivity.class, t);
                showProgress(false);
                showToastMessage("Server Error");
            }
        });
    }

    private void confirmStripePayment() {
        PaymentMethodCreateParams params = mCardInputWidget.getPaymentMethodCreateParams();
        if (params != null) {
            ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
            confirmParams.setReceiptEmail(etEmail.getText().toString());

            Address address = new Address.Builder()
                    .setLine1(etAddress.getText().toString())
                    .setLine2(etAddress2.getText().toString())
                    .setPostalCode(etPostCode.getText().toString())
                    .setCity(etTown.getText().toString())
                    .setCountry("UK")
                    .build();

            ConfirmPaymentIntentParams.Shipping shipping = new ConfirmPaymentIntentParams
                    .Shipping(address, etFirstName.getText().toString() + " " + etLastName.getText().toString(), null, etPhone.getText().toString());
            confirmParams.setShipping(shipping);

            final Context context = getApplicationContext();
            stripe = new Stripe(
                    context,
                    PaymentConfiguration.getInstance(context).getPublishableKey()
            );
            stripe.confirmPayment(this, confirmParams);
        }
    }

    public void placeOrderOnWordPress() {
        if (isFormValidated()) {
            showProgress(true);
            Order order = new Order();
            if (mCash) {
                order.setPaymentMethod("Cash");
                order.setPaymentMethodTitle("Cash");
                order.setSetPaid(true);
            } else {
                order.setPaymentMethod("Pay via Stripe");
                order.setPaymentMethodTitle("Stripe");
                order.setSetPaid(true);
            }

            BillingModel billing = new BillingModel();
            billing.setFistName(etFirstName.getText().toString());
            billing.setLastName(etLastName.getText().toString());
            billing.setAddress1(etAddress.getText().toString());
            billing.setAddress2(etAddress2.getText().toString());
            billing.setCity(etTown.getText().toString());
            billing.setState("");
            billing.setPostCode(etPostCode.getText().toString());
            billing.setCountry("UK");
            billing.setEmail(etEmail.getText().toString());
            billing.setPhone(etPhone.getText().toString());
            order.setBilling(billing);
            order.setShipping(billing);

            ShippingLines shippingLines = new ShippingLines();
            shippingLines.setMethodId("flat_rate");
            shippingLines.setMethodTitle("Delivery");
            shippingLines.setTotal(mDeliveryCharge);

            ShippingLines collection = new ShippingLines();
            collection.setMethodId("local_pickup");
            collection.setMethodTitle("Collection");
            collection.setTotal("0");
            List<ShippingLines> shipping = new ArrayList<>();

            List<CouponLines> couponLines = new ArrayList<>();
            CouponLines coupon = new CouponLines();
            coupon.setCouponCode("10off");
            couponLines.add(coupon);
            order.setCouponLines(couponLines);

            if (mDelivery) {
                shipping.add(shippingLines);
            } else {
                shipping.add(collection);
            }
            order.setShippingLines(shipping);


            List<LineItems> lineItems = new ArrayList<>();
            Cart cart = PaperUtil.getCartItems();
            if (cart != null) {
                List<CartItem> items = cart.getProducts();
                for (int i = 0; i < items.size(); i++) {
                    LineItems item = new LineItems();
                    item.setProductId((int) items.get(i).getProductId());
                    item.setQuantity(items.get(i).getQuantity());
                    item.setName(items.get(i).getName());
                    item.setTotal(String.valueOf(items.get(i).getPrice()));
                    item.setPrice(items.get(i).getOriginalPrice());
                    lineItems.add(item);
                }
            }
            order.setLineItems(lineItems);

            APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
            Call<OrderPlacedResponse> call = service.placeOrder(order, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);

            call.enqueue(new Callback<OrderPlacedResponse>() {
                @Override
                public void onResponse(@NonNull Call<OrderPlacedResponse> call, @NonNull Response<OrderPlacedResponse> response) {
                    if (response.body() != null) {
                        runOnUiThread(() -> addOrderNotes(response.body()));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<OrderPlacedResponse> call, @NonNull Throwable t) {
                    Analytics.log(CheckOutActivity.class, t);
                    showProgress(false);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
    }

    private void addOrderNotes(OrderPlacedResponse orderPlacedResponse) {
        NotesBody notesBody = new NotesBody();
        StringBuilder notes = new StringBuilder();
        Cart items = PaperUtil.getCartItems();
        for (CartItem item : items.getProducts()) {
            notes.append(item.getOptions().toString());
        }
        notesBody.setNotes(notes.toString());

        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<NotesResponse> call = service.addOrderNotes(orderPlacedResponse.getId(), notesBody, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<NotesResponse>() {
            @Override
            public void onResponse(Call<NotesResponse> call, Response<NotesResponse> response) {
                if (response.body() != null) {
                    showToastMessage("Order Placed Successfully!");
                    Intent intent = new Intent(getBaseContext(), ThankYouActivity.class);
                    intent.putExtra("orderDetails", orderPlacedResponse);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                showProgress(false);
            }

            @Override
            public void onFailure(Call<NotesResponse> call, Throwable t) {
                Analytics.log(CheckOutActivity.class, t);
                showProgress(false);
            }
        });
    }

    @Override
    public void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar) {
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        TextView textView = toolbar.findViewById(R.id.tvHeading);
        textView.setText(R.string.check_out);
        ImageButton cartButton = toolbar.findViewById(R.id.cartView);
        cartButton.setVisibility(View.GONE);
    }

    private void calculatePostCodeCharges(String postCode) {
        mPostCodeExist = false;
        mDeliveryCharge = "2";
        CommonUtils utils = new CommonUtils();
        for (String code : utils.area1) {
            if (CommonUtils.postcodeInRange(code, postCode)) {
                mDeliveryCharge = "2.0";
                mPostCodeExist = true;
            }
        }

        for (String code : utils.area2) {
            if (CommonUtils.postcodeInRange(code, postCode)) {
                mDeliveryCharge = "3.0";
                mPostCodeExist = true;
            }
        }

        for (String code : utils.area3) {
            if (CommonUtils.postcodeInRange(code, postCode)) {
                mDeliveryCharge = "4.0";
                mPostCodeExist = true;
            }
        }

        for (String code : utils.area4) {
            if (CommonUtils.postcodeInRange(code, postCode)) {
                mDeliveryCharge = "5.0";
                mPostCodeExist = true;
            }
        }
    }

    private boolean isFormValidated() {
        if (TextUtils.isEmpty(etFirstName.getText().toString())) {
            showToastMessage("Enter First Name");
            return false;
        }
        if (TextUtils.isEmpty(etLastName.getText().toString())) {
            showToastMessage("Enter Last Name");
            return false;
        }
        if (TextUtils.isEmpty(etAddress.getText().toString())) {
            showToastMessage("Enter Address");
            return false;
        }
        if (TextUtils.isEmpty(etAddress2.getText().toString())) {
            showToastMessage("Enter Address Line 2");
            return false;
        }
        if (TextUtils.isEmpty(etTown.getText().toString())) {
            showToastMessage("Enter Town");
            return false;
        }
        if (TextUtils.isEmpty(etPostCode.getText().toString())) {
            showToastMessage("Enter Postcode");
            return false;
        }
        if (TextUtils.isEmpty(etPhone.getText().toString())) {
            showToastMessage("Enter Phone Number");
            return false;
        }
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            showToastMessage("Enter Email Address");
            return false;
        }
        if (!CommonUtils.isEmailValid(etEmail.getText().toString())) {
            showToastMessage("Enter a valid email address");
            return false;
        }
        return true;
    }

    @Override
    public void onAddClick(CartItem cartItem, int position) {

    }

    @Override
    public void onRemoveClick(CartItem cartItem, int position) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
