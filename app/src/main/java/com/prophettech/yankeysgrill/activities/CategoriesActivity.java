/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.adapters.CategoriesAdapter;
import com.prophettech.yankeysgrill.base.BaseActivity;
import com.prophettech.yankeysgrill.models.CategoriesModel;
import com.prophettech.yankeysgrill.rest.data.DataSource;
import com.prophettech.yankeysgrill.rest.data.RemoteDataSource;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

public class CategoriesActivity extends BaseActivity implements CategoriesAdapter.CategoryClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private CategoriesAdapter mAdapter;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_main);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new CategoriesAdapter();
        mRecyclerView.setAdapter(mAdapter);

        getCategories();
    }

    private void getCategories() {
        showProgress(true);
        RemoteDataSource.getInstance().getAllCategories(new DataSource.GetCategoriesCallback() {
            @Override
            public void onFailure(Throwable throwable) {
                Log.d(TAG, Objects.requireNonNull(throwable.getMessage()));
                showProgress(false);
            }

            @Override
            public void onNetworkFailure() {

            }

            @Override
            public void onSuccess(List<CategoriesModel> categories) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    categories.sort(Comparator.comparing(CategoriesModel::getMenuOrder));
                }
                mAdapter.refresh(categories, CategoriesActivity.this);
                showProgress(false);
            }
        });
    }

    @Override
    public void onCategoryClick(double categoryId) {
        Intent intent = new Intent(getBaseContext(), ProductsActivity.class);
        intent.putExtra("CATEGORY_ID", categoryId);
        startActivity(intent);
    }

    @Override
    public void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar) {
        TextView textView = toolbar.findViewById(R.id.tvHeading);
        textView.setText(R.string.categories_title);
    }
}
