/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.prophettech.yankeysgrill.R;

public class ClosedOrders extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closed_orders);
    }
}
