/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.adapters.ChipAdapter;
import com.prophettech.yankeysgrill.adapters.OptionsAdapter;
import com.prophettech.yankeysgrill.base.BaseActivity;
import com.prophettech.yankeysgrill.models.OptionsItemModel;
import com.prophettech.yankeysgrill.models.OptionsModel;
import com.prophettech.yankeysgrill.models.cart.CartItem;
import com.prophettech.yankeysgrill.models.retrofit.APIInterface;
import com.prophettech.yankeysgrill.models.retrofit.MetaDatum;
import com.prophettech.yankeysgrill.models.retrofit.Model;
import com.prophettech.yankeysgrill.models.retrofit.RetrofitClientInstance;
import com.prophettech.yankeysgrill.rest.Services;
import com.prophettech.yankeysgrill.utils.CommonUtils;
import com.prophettech.yankeysgrill.utils.OverlayDialog;
import com.prophettech.yankeysgrill.utils.PaperUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends BaseActivity implements OptionsAdapter.IOptionSelected, ChipAdapter.IChipInterface {

    @BindView(R.id.mealTitle)
    TextView mMealTitle;
    @BindView(R.id.mealDescription)
    TextView mMealDescription;
    @BindView(R.id.spinner)
    Spinner mSpinner;
    @BindView(R.id.spinner2)
    Spinner mSpinner1;
    @BindView(R.id.totalTextView)
    TextView mTotalTextView;
    @BindView(R.id.ivAdd)
    ImageView mAddIV;
    @BindView(R.id.ivRemove)
    ImageView mRemoveIV;
    @BindView(R.id.tvCount)
    TextView mCountTV;
    @BindView(R.id.rvOptionChips)
    RecyclerView rvChips;
    @BindView(R.id.optionsButton)
    Button mOptionsButton;
    @BindView(R.id.optionsTotalTextView)
    TextView mOptionsTotalTextView;
    @BindView(R.id.productImageView)
    ImageView mProductImage;

    private Model mModel;
    private double mTotal = 0;
    private double mQuantity = 0;
    private double mProductPrice = 0;
    private double mOptionsPrice = 0;
    private OptionsModel mOptionsModel = new OptionsModel();
    private Map<String, String> typeAndPriceMap;
    private Map<String, String> sizeAndPriceMap;
    private Map<String, String> optionsMap;
    private Map<String, String> selectedOptions = new ArrayMap<>();
    private ChipAdapter mChipAdapter;

    private OverlayDialog mOverlayDialog;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        onCreate(savedInstanceState, R.layout.activity_detail);
        double mProductId = getIntent().getDoubleExtra("PRODUCT_ID", 0);
        getProductDetail((int) Math.round(mProductId));
        mAddIV.setOnClickListener(v -> {
            if (mQuantity < 99) {
                mQuantity += 1;
                mCountTV.setText(String.valueOf(mQuantity));
                updatePrice();
            }
        });
        mRemoveIV.setOnClickListener(v -> {
            if (mQuantity > 1) {
                mQuantity -= 1;
                mCountTV.setText(String.valueOf(mQuantity));
                updatePrice();
            }
        });
        initViews();
    }

    private void initViews() {
        mChipAdapter = new ChipAdapter(this);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvChips.setLayoutManager(layoutManager);
        rvChips.setAdapter(mChipAdapter);
    }

    private void getProductDetail(int productId) {
        showProgress(true);

        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<Model> call = service.getProductDetails(productId, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);

        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(@NonNull Call<Model> call, @NonNull Response<Model> response) {
                mModel = response.body();
                setData();
                showProgress(false);
            }

            @Override
            public void onFailure(@NonNull Call<Model> call, @NonNull Throwable t) {
                Log.d(TAG, t.getMessage());
                showProgress(false);
                Toast.makeText(getApplicationContext(), "There was an error parsing data!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setData() {
        mMealTitle.setText(mModel.getName());
        mMealDescription.setText(Html.fromHtml(mModel.getDescription()).toString());

        if (mModel.getImages().size() > 0) {
            String url = mModel.getImages().get(0).get("src");
            Picasso.get().load(url).into(mProductImage);
        }

        for (int i = 0; i < mModel.getMetaData().size(); i++) {
            MetaDatum metaData = mModel.getMetaData().get(i);
            if (metaData.getValue() != null && metaData.getValue().getTmfbuilder() != null) {

                if (metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsValue() != null && metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsValue().size() >= 1) {
                    typeAndPriceMap = combineListsIntoOrderedMap(metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsValue().get(0), metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsPrice().get(0));
                    List<String> typeAndPriceList = new ArrayList<>(typeAndPriceMap.keySet());
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, typeAndPriceList);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mSpinner.setVisibility(View.VISIBLE);
                    mSpinner.setAdapter(dataAdapter);
                } else {
                    mSpinner.setVisibility(View.GONE);
                }
                if (metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsValue() != null && metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsValue().size() >= 2) {
                    sizeAndPriceMap = combineListsIntoOrderedMap(metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsValue().get(1), metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsPrice().get(1));
                    List<String> list = new ArrayList<>(sizeAndPriceMap.keySet());
                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mSpinner1.setAdapter(dataAdapter2);
                    mSpinner1.setVisibility(View.VISIBLE);
                } else {
                    mSpinner1.setVisibility(View.GONE);
                }

                if (metaData.getValue().getTmfbuilder().getMultipleCheckboxesOptionsValue() != null) {
                    for (int j = 0; j < metaData.getValue().getTmfbuilder().getMultipleCheckboxesOptionsValue().size(); j++) {
                        optionsMap = combineListsIntoOrderedMap(metaData.getValue().getTmfbuilder().getMultipleCheckboxesOptionsValue().get(j), metaData.getValue().getTmfbuilder().getMultipleCheckboxesOptionsPrice().get(j));
                    }

                    List<OptionsItemModel> options = new ArrayList<>();
                    List<List<String>> data = metaData.getValue().getTmfbuilder().getMultipleCheckboxesOptionsValue();


                    for (int k = 0; k < data.size(); k++) {
                        OptionsItemModel itemModel = new OptionsItemModel();
                        Map<String, String> optionsNewMap = new ArrayMap<>();
                        for (int l = 0; l < data.get(k).size(); l++) {
                            optionsNewMap.put(data.get(k).get(l), metaData.getValue().getTmfbuilder().getMultipleCheckboxesOptionsPrice().get(k).get(l));
                            itemModel.setOptionsMap(optionsNewMap);
                        }
                        options.add(itemModel);
                    }
                    mOptionsModel.setItems(options);
                    Log.d("----------", mOptionsModel.toString());
                    mOptionsButton.setVisibility(View.VISIBLE);
                } else {
                    mOptionsButton.setVisibility(View.GONE);
                }

                if (metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsPrice() != null) {
                    for (List<String> price : metaData.getValue().getTmfbuilder().getMultipleSelectboxOptionsPrice()) {
                        if (price.size() > 0 && !price.get(0).equals("")) {
                            mProductPrice = Double.parseDouble(price.get(0));
                        }
                    }
                }
            }
        }

        if (mProductPrice == 0) {
            mProductPrice = Double.parseDouble(mModel.getPrice());
            updatePrice();
        }

        setPriceBasedOnType();
    }

    private void setPriceBasedOnType() {
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (TextUtils.isEmpty(typeAndPriceMap.get(selectedItem)) || typeAndPriceMap.get(selectedItem).equals("0")) {
                    setPriceBasedOnSize();
                } else {
                    if (typeAndPriceMap.get(selectedItem) != null && Double.parseDouble(typeAndPriceMap.get(selectedItem)) != 0)
                        mProductPrice = Double.parseDouble(typeAndPriceMap.get(selectedItem));
                }
                updatePrice();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void setPriceBasedOnSize() {
        mSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (!TextUtils.isEmpty(sizeAndPriceMap.get(selectedItem))) {
                    if (sizeAndPriceMap.get(selectedItem) != null && Double.parseDouble(sizeAndPriceMap.get(selectedItem)) != 0)
                        mProductPrice = Double.parseDouble(sizeAndPriceMap.get(selectedItem));
                } else {
                    setPriceBasedOnType();
                }
                updateOptions(position);
                updatePrice();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateOptions(int index) {
        OptionsItemModel item = mOptionsModel.getItems().get(index + 1);
        OptionsItemModel constantItem = mOptionsModel.getItems().get(0);
        Map<String, String> options = constantItem.getOptionsMap();
        for (Map.Entry<String, String> itemModel : item.getOptionsMap().entrySet()) {
            options.put(itemModel.getKey(), itemModel.getValue());
        }
        optionsMap = options;
    }

    private void updatePrice() {
        mTotal = ((mProductPrice * mQuantity) + mOptionsPrice);
        mTotalTextView.setText("￡" + CommonUtils.getTwoDecimal(mTotal));
    }

    private void updateOptionsPrice() {
        mOptionsPrice = 0;
        List<String> optionsPrice = new ArrayList<>(selectedOptions.values());
        for (String price : optionsPrice) {
            Double amount = Double.parseDouble(price);
            mOptionsPrice += amount;
        }
        mOptionsTotalTextView.setText("￡" + CommonUtils.getTwoDecimal(mOptionsPrice));
        updatePrice();
    }

    @Override
    public void onToolBarSetUp(Toolbar toolbar, ActionBar actionBar) {
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
    }

    private Map<String, String> combineListsIntoOrderedMap(List<String> keys, List<String> values) {
        if (keys.size() != values.size())
            throw new IllegalArgumentException("Cannot combine lists with dissimilar sizes");
        Map<String, String> map = new LinkedHashMap<>();
        for (int i = 0; i < keys.size(); i++) {
            map.put(keys.get(i), values.get(i));
        }
        return map;
    }

    public void addToCart(View view) {
        if (mQuantity != 0) {
            if (selectedOptions != null && mSpinner.getSelectedItemPosition() != -1 && mSpinner1.getSelectedItemPosition() != -1) {
                selectedOptions.put(mSpinner.getSelectedItem().toString(), mSpinner1.getSelectedItem().toString());
            }
            CartItem cartItem = new CartItem();
            cartItem.setName(mModel.getName());
            cartItem.setProductId(mModel.getId());
            cartItem.setPrice(mTotal);
            cartItem.setQuantity((int) mQuantity);
            cartItem.setOptions(selectedOptions);
            cartItem.setOriginalPrice(mProductPrice);
            cartItem.setOptionsPrice(mOptionsPrice);
            PaperUtil.setCartItem(cartItem);
            Toast.makeText(this, "Item Added To Cart!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Add Quantity!!", Toast.LENGTH_SHORT).show();
        }
    }

    public void showOptions(View view) {
        if (optionsMap != null && optionsMap.size() > 0) {
            OptionsAdapter optionsAdapter = new OptionsAdapter(this);
            mOverlayDialog = new OverlayDialog(this, optionsAdapter, () -> {

                if (mChipAdapter != null && selectedOptions.size() > 0) {
                    enableChipView(true);
                } else {
                    enableChipView(false);
                }
                mChipAdapter.refresh(selectedOptions);
                updateOptionsPrice();
                mOverlayDialog.dismissDialog();
            });
            optionsAdapter.refresh(optionsMap);
            mOverlayDialog.showDialog();
        } else {
            showToastMessage("There are no options for this item.");
        }
    }

    @Override
    public void onCheckboxClicked(String key, String value, boolean isChecked) {
        if (isChecked) {
            selectedOptions.put(key, value);
        }
    }

    private void enableChipView(boolean enable) {
        if (enable) {
            rvChips.setVisibility(View.VISIBLE);
        } else {
            rvChips.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCloseClick(String key, int position) {
        if (mChipAdapter != null) {
            mChipAdapter.removeChip(key);
            updateOptionsPrice();
            if (selectedOptions.size() > 0) {
                enableChipView(true);
            } else {
                enableChipView(false);
            }
        }
    }

    @Override
    public void onChipClick(String key, int position) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
