/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.rest;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.prophettech.yankeysgrill.YankeysGrillApplication;

import org.json.JSONArray;
import org.json.JSONObject;

public class NetworkCaller {

    private final static String TAG = NetworkCaller.class.toString();

    public static void makeRequestForJsonObject(int method, String url, final boolean isForAvoidCache, final JSONObject requestJson, final String tag, Response.Listener<JSONObject> successListner, Response.ErrorListener errorListener) {
        JsonObjectRequest objectRequest = new JsonObjectRequest(method, url, requestJson, successListner, errorListener);
        YankeysGrillApplication instance = YankeysGrillApplication.getInstance();
        instance.addToRequestQueue(objectRequest, tag);
    }

    public static void makeRequestForJsonArray(int method, String url, JSONArray requestJson, Response.Listener<JSONArray> successListner, Response.ErrorListener errorListener, String tag) {
        JsonArrayRequest objectRequest = new JsonArrayRequest(method, url, requestJson, successListner, errorListener);
        YankeysGrillApplication instance = YankeysGrillApplication.getInstance();
        instance.addToRequestQueue(objectRequest, tag);
    }

    public static void makeRequestForString(int method, String url, final String tag, Response.Listener<String> successListener, Response.ErrorListener errorListener) {
        StringRequest request = new StringRequest(method, url, successListener, errorListener);
        YankeysGrillApplication instance = YankeysGrillApplication.getInstance();
        instance.addToRequestQueue(request, tag);
    }
}
