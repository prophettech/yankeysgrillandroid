/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.rest;

public class Services {

    public static final String CONSUMER_SECRET = "cs_039e3a8e9eced1f51445ca15cd3330bbff53ea80";
    public static final String CONSUMER_KEY = "ck_2e74e3bd01e76a870edd4eafb4298ea848e27a77";

    public static final String BASE_URL = "https://yankeysgrill.co.uk/";
    public static final String GET_ALL_PRODUCTS = "wp-json/wc/v3/products";
    public static final String GET_ALL_CATEGORIES = "wp-json/wc/v3/products/categories";
    public static final String GET_PRODUCTS_BY_CATEGORY = "wp-json/wc/v3/products?per_page=100&category=";
    public static final String GET_PRODUCT_BY_ID = "wp-json/wc/v3/products/";
    public static final String PLACE_ORDER = "wp-json/wc/v3/orders";
    public static final String ORDER_STATUS = "fcm/orders.json";
}
