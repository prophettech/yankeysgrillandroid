/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.rest;

import android.util.Log;

public class URLBuilder {

    private static String TAG = URLBuilder.class.getSimpleName();

    public static String getURLWithKeys(String service) {
        StringBuilder builder = new StringBuilder(Services.BASE_URL);
        builder.append(service);
        builder.append("?consumer_key=");
        builder.append(Services.CONSUMER_KEY);
        builder.append("&consumer_secret=");
        builder.append(Services.CONSUMER_SECRET);
        String url = builder.toString();
        Log.d(TAG, url);
        return url;
    }

    public static String getCategoriesURL(String service) {
        StringBuilder builder = new StringBuilder(Services.BASE_URL);
        builder.append(service);
        builder.append("?per_page=100&consumer_key=");
        builder.append(Services.CONSUMER_KEY);
        builder.append("&consumer_secret=");
        builder.append(Services.CONSUMER_SECRET);
        String url = builder.toString();
        Log.d(TAG, url);
        return url;
    }

    public static String getProdcutsWithIdURL(String service, double id) {
        StringBuilder builder = new StringBuilder(Services.BASE_URL);
        builder.append(service);
        builder.append("?per_page=100&category=");
        builder.append(id);
        builder.append("&consumer_key=");
        builder.append(Services.CONSUMER_KEY);
        builder.append("&consumer_secret=");
        builder.append(Services.CONSUMER_SECRET);
        String url = builder.toString();
        Log.d(TAG, url);
        return url;
    }

    public static String getOrderStatusURL(String service) {
        StringBuilder builder = new StringBuilder(Services.BASE_URL);
        builder.append(service);
        String url = builder.toString();
        Log.d(TAG, url);
        return url;
    }

    public static String getProductById(String service, int id) {
        StringBuilder builder = new StringBuilder(Services.BASE_URL);
        builder.append(service);
        builder.append(id);
        builder.append("?consumer_key=");
        builder.append(Services.CONSUMER_KEY);
        builder.append("&consumer_secret=");
        builder.append(Services.CONSUMER_SECRET);
        String url = builder.toString();
        Log.d(TAG, url);
        return url;
    }
}
