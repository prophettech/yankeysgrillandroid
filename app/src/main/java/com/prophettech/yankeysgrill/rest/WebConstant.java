/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.rest;

import com.android.volley.Request;

public class WebConstant {
    public static int GET = Request.Method.GET;
    public static int POST = Request.Method.POST;

    public static String SUCCESS_CODE = "1";
    public static String FAILURE_CODE = "0";
}
