/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 13:03
 *
 */

package com.prophettech.yankeysgrill.rest.data;

import com.prophettech.yankeysgrill.models.CategoriesModel;
import com.prophettech.yankeysgrill.models.ImageModel;
import com.prophettech.yankeysgrill.models.ProductsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResponseHandler {

    private static String TAG = ResponseHandler.class.getSimpleName();

    static List<CategoriesModel> getAllCategories(JSONArray data) {

        if (data == null) {
            return null;
        }

        List<CategoriesModel> categoriesList = new ArrayList<>();

        for (int i = 0; i < data.length(); i++) {

            CategoriesModel category = new CategoriesModel();
            JSONObject eachCategory = data.optJSONObject(i);
            category.setId(eachCategory.optDouble("id"));
            category.setName(eachCategory.optString("name"));
            category.setDescription(eachCategory.optString("description"));
            category.setCount(eachCategory.optDouble("count"));
            category.setMenuOrder(eachCategory.optInt("menu_order"));
            ImageModel image = new ImageModel();
            JSONObject imageObject = eachCategory.optJSONObject("image");
            if (imageObject != null) {
                image.setId(imageObject.optDouble("id"));
                image.setSrc(imageObject.optString("src"));
            }
            category.setImage(image);

            if (eachCategory.optDouble("count") != 0) {
                categoriesList.add(category);
            }
        }

        return categoriesList;
    }

    static List<ProductsModel> getProducts(JSONArray data) {

        if (data == null) {
            return null;
        }

        List<ProductsModel> productsList = new ArrayList<>();

        for (int i = 0; i < data.length(); i++) {

            ProductsModel product = new ProductsModel();
            JSONObject eachProduct = data.optJSONObject(i);
            product.setId(eachProduct.optDouble("id"));
            product.setName(eachProduct.optString("name"));
            product.setDescription(eachProduct.optString("description"));
            product.setMenuOrder(eachProduct.optInt("menu_order"));

            ImageModel image = new ImageModel();
            JSONArray imageObject = eachProduct.optJSONArray("images");
            if (imageObject != null) {
                for (int j = 0; j < imageObject.length(); j++) {
                    JSONObject eachImage = imageObject.optJSONObject(j);
                    image.setSrc(eachImage.optString("src"));
                    image.setId(eachImage.optDouble("id"));
                }
            }

            product.setImage(image);
            productsList.add(product);
        }

        return productsList;
    }

}
