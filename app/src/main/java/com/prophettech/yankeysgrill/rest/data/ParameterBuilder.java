/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.rest.data;

import com.prophettech.yankeysgrill.models.billing.LineItems;
import com.prophettech.yankeysgrill.models.billing.Order;
import com.prophettech.yankeysgrill.models.billing.ShippingLines;
import com.prophettech.yankeysgrill.utils.Analytics;

import org.json.JSONArray;
import org.json.JSONObject;

public class ParameterBuilder {


    static JSONObject buildOrder(Order order) {
        JSONObject param = new JSONObject();
        try {
            param.put("payment_method", order.getPaymentMethod());
            param.put("payment_method_title", order.getPaymentMethodTitle());
            param.put("set_paid", order.isSetPaid());

            JSONObject billingObject = new JSONObject();
            billingObject.put("first_name", order.getBilling().getFistName());
            billingObject.put("last_name", order.getBilling().getLastName());
            billingObject.put("address_1", order.getBilling().getAddress1());
            billingObject.put("address_2", order.getBilling().getAddress2());
            billingObject.put("city", order.getBilling().getCity());
            billingObject.put("state", order.getBilling().getState());
            billingObject.put("postcode", order.getBilling().getPostCode());
            billingObject.put("country", order.getBilling().getCountry());
            billingObject.put("email", order.getBilling().getEmail());
            billingObject.put("phone", order.getBilling().getPhone());
            param.put("billing", billingObject);

            JSONObject shippingObject = new JSONObject();
            shippingObject.put("first_name", order.getBilling().getFistName());
            shippingObject.put("last_name", order.getBilling().getLastName());
            shippingObject.put("address_1", order.getBilling().getAddress1());
            shippingObject.put("address_2", order.getBilling().getAddress2());
            shippingObject.put("city", order.getBilling().getCity());
            shippingObject.put("state", order.getBilling().getState());
            shippingObject.put("postcode", order.getBilling().getPostCode());
            shippingObject.put("country", order.getBilling().getCountry());
            shippingObject.put("email", order.getBilling().getEmail());
            shippingObject.put("phone", order.getBilling().getPhone());
            param.put("shipping", shippingObject);

            JSONArray lineItems = new JSONArray();
            for (LineItems item : order.getLineItems()) {
                JSONObject lineObject = new JSONObject();
                lineObject.put("name", item.getName());
                lineObject.put("product_id", item.getProductId());
                lineObject.put("quantity", item.getQuantity());
                lineObject.put("total", item.getTotal());
                lineObject.put("price", item.getPrice());
                lineItems.put(lineObject);
            }
            param.put("line_items", lineItems);

            JSONArray shippingLines = new JSONArray();
            if (order.getShippingLines() != null) {
                for (ShippingLines item : order.getShippingLines()) {
                    shippingLines.put(item);
                }
            }
            param.put("shipping_lines", shippingLines);
        } catch (Exception e) {
            helperLog(e);
        }
        helperLog(param);
        return null;
    }


    private static void helperLog(JSONObject json) {
        if (json != null) {
            Analytics.log(ParameterBuilder.class, json.toString());
        }
    }

    private static void helperLog(Throwable e) {
        Analytics.log(ParameterBuilder.class, e);
    }

}
