/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:19
 *
 */

package com.prophettech.yankeysgrill.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {

    private static final String TAG = CommonUtils.class.getSimpleName();

    public static boolean isNetworkAvailable(Context context) {
        boolean isNetwork = false;
        if (context != null) {
            try {
                ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = null;
                if (connMgr != null) {
                    networkInfo = connMgr.getActiveNetworkInfo();
                }
                if (networkInfo != null && networkInfo.isAvailable()
                        && networkInfo.isConnected()) {
                    isNetwork = true;
                }
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
        }

        return isNetwork;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.UNNECESSARY);
        return bd.doubleValue();
    }

    public static String getTwoDecimal(double val) {
        DecimalFormat df = new DecimalFormat("#######0.00");
        df.setMinimumFractionDigits(2);
        String inter = df.format(val);
        //double output =  Double.valueOf(inter).doubleValue();
        return inter;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidPostCode(String postCode) {
        String expression = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(postCode);
        return matcher.matches();
    }

    public String[] zone1 = {"DH3"};

    public String[] zone2 = {"NE38", "NE9", "NE11"};

    public String[] zone3 = {"DH5", "DH4", "DH2", "DH1", "NE8", "NE10"};

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }


    public static String getUserAgent() {
        return "Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> " +
                "(KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>";
    }

    public static boolean postcodeInRange(String regex, String postcode) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(postcode);
        return matcher.matches();
    }

    public String[] area1 = {"NE23(?:.*)"};

    public String[] area2 = {"NE25(?:.*)"};

    public String[] area3 = {"NE13(?:.*)", "NE27(?:.*)"};

    public String[] area4 = {"NE26(?:.*)", "NE24(?:.*)", "NE62(?:.*)"};


}
