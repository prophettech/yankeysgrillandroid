/*
 * *
 *  * Created by Mohammed Akram Hussain on 03/10/20 13:03
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 03/10/20 12:16
 *
 */

package com.prophettech.yankeysgrill.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.yankeysgrill.R;
import com.prophettech.yankeysgrill.adapters.OptionsAdapter;

public class OverlayDialog {

    private Dialog dialog;

    private TextView btPositive;
    private TextView btNegative;
    private TextView tvHeading;
    private TextView tvMessage;
    private EditText etInput;
    private Activity context;
    private RecyclerView recyclerView;

    public OverlayDialog(Activity context, OptionsAdapter adapter, IOptionUpdated listener) {
        this.context = context;
        if (context != null && !context.isFinishing()) {
            context.runOnUiThread(() -> {
                dialog = new Dialog(context, R.style.TransparentLoadingAnimation);
                dialog.setContentView(R.layout.layout_options);
                dialog.getWindow().setLayout(((getWidth(context) / 100) * 90), LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.setCancelable(false);
                btPositive = dialog.findViewById(R.id.btUpdate);
                btNegative = dialog.findViewById(R.id.btCancel);
                recyclerView = dialog.findViewById(R.id.rvOptions);
                recyclerView.setHasFixedSize(true);
                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);

                btPositive.setOnClickListener(v -> {
                    if (listener != null) {
                        listener.onOptionUpdated();
                    }
                });

                btNegative.setOnClickListener(v -> dialog.dismiss());
            });
        }
    }

    public OverlayDialog(Activity context, View.OnClickListener positiveClick, View.OnClickListener negativeClick, String heading, String message, String positiveButton, String negativeButton) {
        this.context = context;
        if (context != null && !context.isFinishing()) {
            context.runOnUiThread(() -> {
                dialog = new Dialog(context, R.style.TransparentLoadingAnimation);
                dialog.setContentView(R.layout.confirm_dialog);
                dialog.setCancelable(false);
                tvHeading = dialog.findViewById(R.id.tvHeading);
                tvMessage = dialog.findViewById(R.id.tvMessage);
                btPositive = dialog.findViewById(R.id.btPositive);
                btNegative = dialog.findViewById(R.id.btNegative);
                btPositive.setOnClickListener(positiveClick);
                btNegative.setOnClickListener(negativeClick);

                tvHeading.setText(heading);
                tvMessage.setText(message);
                btNegative.setText(negativeButton);
                btPositive.setText(positiveButton);
            });
        }

    }

    public OverlayDialog(Activity context) {
        this.context = context;
        if (context != null && !context.isFinishing()) {
            context.runOnUiThread(() -> {
                dialog = new Dialog(context, R.style.TransparentLoadingAnimation);
                dialog.setContentView(R.layout.overlay_dialog_layout);
                tvHeading = dialog.findViewById(R.id.tvHeading);
                tvMessage = dialog.findViewById(R.id.tvMessage);
                btPositive = dialog.findViewById(R.id.btPositive);
                btPositive.setOnClickListener(v -> dialog.dismiss());
            });
        }
    }

    private static int getWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowmanager != null) {
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.widthPixels;
    }

    public void setHeading(String heading) {
        tvHeading.setText(heading);
    }

    public void setButtonText(String button) {
        btPositive.setText(button);
    }

    public void setMessage(String message) {
        if (etInput != null) {
            etInput.setHint(message);
            return;
        }
        tvMessage.setText(message);
    }

    public void showDialog() {
        if (context != null && !context.isFinishing()) {
            context.runOnUiThread(() -> dialog.show());
        }
    }

    public void dismissDialog() {
        if (context != null) {
            context.runOnUiThread(() -> dialog.dismiss());
        }
    }

    public boolean isVisible() {
        return dialog != null && dialog.isShowing();
    }

    public interface IOptionUpdated {
        void onOptionUpdated();
    }
}
